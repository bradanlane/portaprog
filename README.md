# PortaProg

A portable programmer for both SPI and UPDI programmed microcontrollers.

This project has been developed for the TTGO T-Display board with a ST7789 display.
A future version may be created for the Adafruit ESP32 Feather.

![Portable Programmer](https://gitlab.com/bradanlane/portaprog/-/raw/master/files/portaprog.jpg)

These affordable boards are then augmented with a PortaProg receiver PCB which adds button, a power switch, necessary passives, and a breakout header.
 *(see `pins.h` documentation for details.)*

Supports:

- a common command set for ISP and UPDI programming
- direct firmware flash, dump, and read/write fuses over TCP
- flash using firmware stored on the SPIFFS
- TCP messages to the device UART (may be stored as command scripts)
- command scripts from SPIFFS (useful for smoketests)
- display of device UART data
- config file support
- startup script option
- file system and menu navigation using a 3-way toggle button
- actions buttons with script assignments

The combination of buttons to run command scripts and the send/receive of strings to a device attached to the UART allows for lots of options including running smoke tests and demos for attached devices.

The PortaProg will look for the `.config` file on the SPIFFS to perform initialization of WiFi, UART, and assignable CMD buttons.
If no `.config` file is found or if the specified WiFi is not available, the PortaProg will establish itself as an AP hotspot.

*NOTE: The PortaProg hardware is also suitable for building IoT devices or small portable games. The 2x6 pin header may be removed if not needed.*

**DISCLAIMER**: The PortaProg code evolved considerably as it was put into production for programming and testing the initial target projects.
During this phase, the priority was to keep those projects on schedule and this *may* have lead to some kludges and butchery of the PortaProg source code.
If you have a PortaProg, and wish to extend this code, you may discover it has some warts. You are free to fork the code. You are also encouraged to submit pull requests.

--------------------------------------------------------------------------

--------------------------------------------------------------------------

## COMMAND SET

The PortaProg has an extensive command set for interacting with attached AVR chips.
The commands abstract the difference between chips using SPI and chips using UPDI for programming.

- SPI is used for ATTiny and ATMega chips
- UPDI is used for tinyAVR and megaAVR chips

The interface processes an incoming stream and dispatches to the SPI or UPDI specific commands.
The chip type may be specified by command or dynamically determined.
_Specifying the chip type is faster as it saves the time required to interrogate the protocols._

The command interface Stream may be from any source and has been developed using a TCP Stream, SPIFFS File Stream, Action buttons, and an internal Buffer/Screen Stream.
- The file Stream is used to process CMD files from SPIFFS.
- The Buffer/Screen Stream is tied to the device UI and is used to perform the self-contained FLASH function from the device UI.
- This Stream is also used when running command sets stored on the SPIFFS.
- There are 3 actions buttons which may be assigned scripts

*(The examples in the documentation use the TCP Stream.)*

The TCP Stream commands are sent to the programmer using the IP and PORT of the PortaProg.
*(The current `IP address` and `port` are displayed on the display during the startup process.)*

The programming and flash processes display progress messages on the PortaProg display and - in teh case of TCP - completion message(s) back to the caller.

The command set interacts with the attached device, a memory buffer, SPIFFS storage, and a connected stream.

The supported operations work between any two endpoints at a time.

*Some common operations require multiple commands. This was chosen to maximize flexibility while keeping the command set to a reasonable size. An example is to program an attached device using a firmware file on the local computer. This operation uses a TCP connection to first `SEND` the firmware HEX file from the local computer to the PortaProg memory buffer; followed by the `FLASH` command to program the attached device with the firmware in the PortaProg memory buffer. Because the `SEND` and `FLASH` are separate, it is easy to change devices and repeat the `FLASH` quickly without reloaded the firmware.*

The Commands may be provided as independent lines or chained together as a single line operation. The exception to this is a _Stream_ parameter which assumes carrage returns and line feeds are part of the data.

**NOTE:** The POWER commands affect the SPI and UPDI power pins.
The implementation controls a MOSFET on the sGND side because an attached device may receive enough power through UART or other pins to keep the chip active.
If you want an always-on power circuit, use the VCC pin in conjunction with the GND located as part of the UART pin set.

--------------------------------------------------------------------------


### HELP Command

**This command is here because everyone needs a little help now and then**

  - `HELP`: returns the full help text of the PortaProg

### SETUP Command

**These commands are used to setup communications with the attached device**

  - `DETECT`: attempts to interrogate both interfaces for an attached device and return what is detected. SPI, UPDI, Both, or Neither. This command is useful for determine proper connections to a chip.

  - `INFO`: attempts to interrogate the attached device(s) and return basic details of chip type, available memories, etc.

  - `UPDI`: configure the PortaProg to assume UPDI for subsequent commands.

  - `SPI`: configure the PortaProg to assume SPI for subsequent commands.

  - `UART`: Set the baud rate of the _UART to attached device_ interface. This interface allows the attached device to send text data to the PortaProg which is displayed on its screen.

### FILE and BUFFER Command

**These commands are used for read/write operations to the file system and the memory buffer**

  - `DIR`: List all files currently stored on the SPIFFS.

  - `DEL` <filename>: Delete the named file from the SPIFFS.

  - `CAT` <filename>: stream contents of file from the SPIFFS back to the local computer standard output.

  - `UPLOAD` <filename> (stream): create a new file in the SPIFFS and store the contents of the stream to the file.
	The `stream` is whatever content is send over TCP to the PortaProg. A common method is to pipe the standard output of a command to TCP.
	For example, to send the contents of a file to standard output, use the Linux `cat` command or the windows `type` command.
  	This is a general purpose file operation and no analysis is performed to determine the contents.
	The primary use of the `UPLOAD` command is to store `CMD` files on the SPIFFS but it is available to store any file.
	_NOTE: The command will consume all data available from the stream.	It is not possible to include a follow-up command on the same TCP command-line. Subsequent commands may be send to the PortaProg using a new command-line._

  - `SEND`: Send a stream of Intel HEX (I8HEX) format text from the local computer to the PortaProg which converts it to binary data and stores it in the memory buffer.
  	This is typically used to upload firmware which may be flashed to a chip with a subsequent command.
	The I8HEX data format has an explicit _end of data_ marker so it is possible to follow the `SEND` command with another command on the same line. _(see the examples below)_

  - `RECEIVE`: Convert the contents of the memory buffer to Intel HEX (I8HEX) format and stream back the resulting text to the local computer standard output.

  - `LOAD` <filename>: Load the specified file from the SPIFFS into memory.
  	If the file extension is `HEX` then the file contents are assumed to be in Intel HEX (I8HEX) format
	and are converted to binary and stored in the memory buffer and available to subsequent commands.
	If the file extension is `CMD` then the file contents are processed as if they were TCP commands.
	_Note: This capability would rarely be used from the TCP command line and is intended to allow a series of commands to be initiated from the PortaProg interface._

  - `STORE` <filename>: Convert the contents of the memory buffer into Intel HEX (I8HEX) format and store as a new file to the SPIFFS.

### AVR Commands

**These commands are used for read/write operations to the attached device**

  - `FUSES`: report back the values of the fuses from the chip.

  - `FUSESET` nn:XX: Set fuse number `nn` to hex value `XX`.
  	_WARNING: No attempt is made to verify the new value. Sending a bad value may render the chip unusable. For this reason, the `lock` fuse may not be changed using the PortaProg._

  - `ERASE`: Initialize the attached device program memory space. This will obviously wipe out any existing program on the chip. _(with great power, comes great responsibility)_

  - `WRITE`: flash the attached device using the contents of the memory buffer.
  	The chip type should be specified prior to using the `WRITE` command - either using the `UPDI` or `SPI` commands
	or using `DETECT` to automatically set the connected chip type.

  - `READ`: read aka dump the contents of the attached device flash memory to the memory buffer.
  	The operation will dump the entire contents of the chip's program memory and
	then attempt to determine the actual size. The result will be rounded up to a _page boundary_ based on the chip's flash memory page size.
	Because the operation must dump the entire program memory, this operation will take longer on larger chips, regardless of the actual size
	of the program.

  - `EWRITE`: write the contents of the memory buffer to the the EEPROM of the attached device.
  	The chip type should be specified prior to using the `EWRITE` command - either using the `UPDI` or `SPI` commands
	or using `DETECT` to automatically set the connected chip type.

  - `EREAD`: Read the contents of the attached device eeprom to the memory buffer.
  	The operation will copy the entire attempt contents of the chip's program memory

  - `POWERON`: enable 3.3V programmer VCC+sGND circuit.

  - `POWEROFF`: disable 3.3V programmer VCC+sGND circuit. It may be helpful to disable power after programming when programming raw chips.

  - `POWERTOGGLE`: toggle state of 3.3V programmer VCC+sGND circuit. This is a UI convenience so only one button is required.

  - `B1SCRIPT`: update Button 1 command string.

  - `B2SCRIPT`: update Button 2 command string.

  - `B3SCRIPT`: update Button 3 command string.

### SCRIPT Commands

**This commands are used for developing automation scripts.**

  - `CLEAR`: Clear the PortaProg screen.
	This can be used in conjunction with TEXT and other commands to develop PortaProg smoke test scripts.

  - `TEXT` (stream): Display text on PortaProg screen. The command supports embedding newlines in the message.
	This can be used in conjunction with WAIT, BUTTON, or TEST to develop PortaProg smoke test scripts.

  - `UTEXT` (stream): Send a text message through the UART connection to the attached device. The command supports embedding newlines in the message.
  	The command will pause for the 'UART timeout' at any newline to give the attached device an opportunity to respond with text output.
	All text received back from the chip is streamed back. The contents of the `UTEXT` send and receive are also displayed on the PortaProg screen.

  - `UCLEAR`: Flush all UART data from the attached device and clear buffer used by `TEST`.
	This can be used in conjunction with and uploaded `CMD` file to develop PortaProg smoke test scripts.

  - `WAIT`: Cause execution to pause for the specified length of milliseconds.
	This is intended for use in an uploaded `CMD` file to develop PortaProg smoke test scripts.

  - `BUTTON`: Cause execution to pause until the NAV Select button has been pressed.
    A short press will continue with any subsequent commands. A long press will abort and ignore any subsequent commands.
	This is intended for use in an uploaded `CMD` file to develop PortaProg smoke test scripts.

  - `TEST` <timeout ms> <abort 0:1> (stream): Compare text received from the attached device for the existence of the _stream_ text - wait for up to _timeout_ milliseconds.
  	If the _stream_ text is not detected **and** if the _abort_ is 1 , then all subsequent non-system commands are ignored.
	This is intended for use in an uploaded `CMD` file to develop PortaProg smoke test scripts.

  - `MUTEON`: Reduce PortaProg status messaging.
    Some PortaProg commands and operations provide status messages on the device display. These messages may not be helpful while executing scripts.
	This command will suppress those messages.

  - `MUTEOFF`: Restore PortaProg status messaging.
    Some PortaProg commands and operations provide status messages on the device display. These messages may not be helpful white executing scripts.
	This command will restore those messages if `MUTEON` has been performed.


--------------------------------------------------------------------------

#### Examples

**NOTE 1**: the following commands assume a Linux command line. Similar command sequences are possible on Windows.
**NOTE 2**: the `IP` and `PORT` values are shown on the PortaProg screen when it is powered on.

- Program a chip using a firmware file from the local computer
	- `(echo 'receive'; cat file.hex; echo 'write') | nc IP PORT`
- Dump the program memory of a chip to a HEX file on the local computer
	- `(echo 'read'; echo 'send') | nc IP PORT > file.hex`
- Upload firmware file `file.hex` to SPIFFS
	- `(echo 'send'; cat file.hex; echo 'store file.hex') | nc IP PORT",
- Send text commands over a UART connection to the program running on attached device
  	_(assumes the running program has an active `Serial` connection open and understand the messages -
	in this example, the program on the chip understands the commands `LED`, `ON`, and `OFF` followed by a number and responds with the state of the LED)_
	- `echo -e 'UTEXT LED 1\nON 2\nOFF 1' | nc IP PORT`
- Update config *(the local file name does not need to be the same as the file stored to SPIFFS)*
	- (echo 'upload .config'; cat config_file) | nc IP PORT'

 #### Button Examples

- Assign a button to load firware from SPIFFS to memory and program a chip from memory
	- B1SCRIPT LOAD firmware.hex WRITE
- Assign a button to program a chip using the firware already in memory _(this is handy for repeat programming tasks)_
	- B2SCRIPT WRITE

#### Script Examples

The following examples are from the ECCCore smoketest and includes comments for each line. _(Note: script files do not support comments)_

In this example, the `.config` file runs `startfile.cmd`. The `startfile.cmd` preloads the firmware and assigns scripted commands to the 3 programmable buttons.

|     File     | Contents    | Description    |
|:------------:|-------------|----------------|
| `.config`    | {<br>"ssid": "WIFI",<br>"password":&nbsp;"wifi-password",<br>"port":&nbsp;8888,<br>"baud":&nbsp;9600,<br>"timeout":&nbsp;500,<br>"startfile":&nbsp;"startfile.cmd"<br>}  | the important JSON data here is the `startfile` assignment which will be run when the PortaProg starts up  |
| `startfile.cmd` | POWEROFF<br>UPDI<br>LOAD&nbsp;smoketest.hex<br>B1SCRIPT&nbsp;LOAD&nbsp;smoketest.cmd<br>B2SCRIPT&nbsp;CAT&nbsp;smoketest.cmd<br>B3SCRIPT&nbsp;POWERTOGGLE  | The script insures power has been turned off to the attached device<br>it sets the programming mode to UPDI<br>it preloads the firmware file from SPIFFS into memory<br>it assigns the `smoketest.cmd` script to run when button 1 is pressed<br><br>for convenience it assigned the power toggle feature to button 3 |
| `smoketest.cmd` | UCLEAR<br>POWEROFF<br>UPDI<br>CLEAR<br>WRITE<br>MUTEON<br>CLEAR<br>POWERON<br>TEST&nbsp;5000&nbsp;1&nbsp;BUZZER<br>UTEXT&nbsp;UART<br>TEST&nbsp;6000&nbsp;1&nbsp;SMOKETEST<br>WAIT&nbsp;500<br>POWEROFF<br>MUTEOFF | the script starts by attempting to clear any data on the UART connection to the attached device<br>it insures the PortaProg is not powering the device<br>it sets the programming mode to UPDI<br>it clears the PortaProg screen<br>it writes the firmware from memory to the device chip<br>it mutes any status messages the PortaProg may normally send to its screen _(this is so the PortaProg screen only shows messages coming from the attached device)_<br>it clears the PortaProg screen in preparation for executing the device smoketest<br>it powers the attached device on _(which immediately start to execute its smoketest code)_<br>it waits up to 5 seconds for the text `BUZZER` to be received over the UART from the device; it will abort the rest of the script if the text is not received.<br>it sends the text `UART` to the device _(the devcie smoketest is validates the UART connection if it receives this text)_<br>it waits up to 6 seconds for the device smoketest to send the text `SMOKETEST` _(which occurs at the end of the device code)_; it will abort the rest of the script if the text is not received.<br>it waits another 1/2 second<br>it then shuts off power to the device<br>it restores PortaProg to display status messages |
--------------------------------------------------------------------------


#### avrPowerOn() / avrPowerOff()

Provide 3.3V across the VCC/sGND circuit.

_The PROGRAM PWR is controllable. However, since there may be power bleed through on UART or UPDI pins, we actual switch the GND.
It may be used for cases where the chip should not execute its own code outside of the AVR API functions._


--------------------------------------------------------------------------

## CONFIG

A JSON syntax config file with each setting on a line and the order is assumed. There is no error checking.

The Order is:
|     Parameter     | Type    | Description                                      |  Required  |
|:-----------------:|---------|--------------------------------------------------|:----------:|
| **ssid**          | string  | SSID of the access point to connect              |  required  |
| **password**      | string  | Password of the access point to connect          |  required  |
| **port**          | integer | TCP Port used in conjunction with the IP address | *optional* |
| **baud**          | integer | Serial baud rate for the connected device        | *optional* |
| **timeout**       | integer | Timeout waiting for serial data from the device  | *optional* |
| **power**         | boolean | Inital state of power for AVR programming        | *optional* |
| **startfile**     | string  | SPIFFS filename to load and run on startup       | *optional* |
| **b1script**      | string  | Command string to run on Actions:1 press         | *optional* |
| **b2script**      | string  | Command string to run on Actions:2 press         | *optional* |
| **b3script**      | string  | Command string to run on Actions:3 press         | *optional* |


The config file name must be `.config` and loaded to the SPIFFS.
The file may be loaded using TCP command set.

The command (from a Linux computer) is:
  - `(echo "upload .config"; cat <your config file>;) | nc <PortaProg IP> <Port>`

The command (from a Windows computer with netcat installed) is:
  - `(echo upload .config & type config_file) | netcat <PortaProg IP> <Port>`

The PortaProg receiver board has two 3-way button controls. One is for navigation and the other is for actions.
The config file allows assignment of scripted commands to the actions buttons.
If no Action button assignments have been provided, then the Action:Select will default to displaying the system Help.

*When the PortaProg is first started and no WiFi has been configured, it will create a hotspot for itself. The default IP address of the hotspot is `192.168.4.1` and the default TCP port is `8888`.*


--------------------------------------------------------------------------

## HARDWARE

The PortaProg uses the LILYGO TTGO T-Display with an enhancement receiver board.

The LILYGO TTGO T-Display has a 240x135 ST7789V display and support for a LiPo battery connected through a JST PH 2.0 connector.

The receiver board breaks into the power management circuit and adds a power off switch.
A 3-way lever button switch provides for menu navigation and scrolling of the viewport.
The PortaProg programming and UART capabilities are exposed through a standard dual row pin header.

An additional 3-way lever button switch is available for scripted actions.
These buttons may be assigned script command strings to execute when pressed.
The 3 buttons are assigned command strings through the `.config` file as `b1script`, `b2script`, and `b3script`.

**NOTE:** The receiver board has a 4.7K Ohm resistor for the UPDI programming interface. It also has pullup resistors to make the GPI34..39 usable as input for the buttons.

*For historical value ... * During the development of the PortaProg one notable issue was with GPIO25.
Use of this pin caused crashes, Seria11.end() to hang, and I2C errors.
The issue with GPIO 25 and 26 is the DAC. To use GPIO25 and GPIO26, the DAC must first be disabled.


## GPIO PIN REQUIREMENTS

The ESP32 has three hardware serial (UART) interfaces. The PortaProg uses all three:
 - Serial0 is used to interface to the computer through the USB (use the default serial for this) for debugging PortaProg
 - Serial1 is used to support UART communications to an attached chip - useful if your program uses UART for debugging or features (this serial interface is accessible thru the PortaProg's command set and over a Telnet connection)
 - Serial2 is used for UPDI - a 4.7K resistor is connected between TX & RX and the RX pin (*the unified pin*) connects to UPDI of the chip

The ESP32 alternate SPI is used to interface to ATTiny and ATMega chips.

The Receiver board has a 3-way navigation for menus and for scrolling the viewport. The center long press will **Select** the menu option or act as an **Return** from text mode.

The TTGO T-Display has 2-buttons on the board. For convenience, these are replicated to the receiver board as action buttons **up** and **down**.

In cases where the chip should be powered off after programming, There are `POWER` commands within the command set.


