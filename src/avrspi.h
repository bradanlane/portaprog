// -------- AVR SPI functions --------------------------------------------------------------------
bool spiInit();
void spiLoop();
bool spiIsConnected(Stream* client, bool silent);
bool spiGetInfo(Stream *client);
bool spiReadFuses(Stream *client);
bool spiWriteFuse(Stream *client, uint8_t fuse_number, uint8_t fuse_value);
bool spiFlashData(Stream *client, ihexStream* src);
bool spiDumpFlash(Stream *client, ihexStream* dest);
bool spiErase(Stream *client);
bool spiWriteEeprom(Stream *client, uint32_t offset, ihexStream* src);
bool spiReadEeprom(Stream *client, uint32_t offset, uint32_t size, ihexStream* dest);
