/* ***************************************************************************
* File:    cmds.cpp
* Date:    2020.08.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ***************************************************************************** */

/* ---
--------------------------------------------------------------------------

Internally, each protocol implements the same set of functions with the same signature.
The functions begin the protocol - 'spi' or 'updi' and then the function name.

#### *protocol*Init()

Perform any initialization necessary for the protocol and the hardware interface.

return: **bool** `true` on success and `false` on failure

#### *protocol*Loop()

Perform any periodic tasks necessary for continued operation.

#### *protocol*IsConnected()

Test is the connection to the chip and that the chip is available for further actions.
supported provided it implements read(), write(), and all print...() methods.

input:

- client **Stream ptr** an active Stream for output of status messages
- silent **bool** a flag to indicate if output should be suppressed

return: **bool** `true` on success and `false` on failure

#### *protocol*GetInfo()

Interrogate the chip and report its basic information.

input: client **Stream ptr** an active Stream for output of status messages

return: **bool** `true` on success and `false` on failure

#### *protocol*ReadFuses()

Access the chip and read its fuses. The available fuses and their meaning is chip specific.
Consult the datasheet for details and descriptions.

input: client **Stream ptr** an active Stream for output of status messages

return: **bool** `true` on success and `false` on failure

#### *protocol*WriteFuses()

Access the chip and read its fuses. The available fuses and their meaning is chip specific.
Consult the datasheet for details and descriptions.

The function will report the fuse values before and after the operation.

input:

- client **Stream ptr** an active Stream for output of status messages
- number **uint8_t** the numerical index of the fuse in the sequence specific to the chip.
- value **uint8_t** the new value for the fuse.

return: **bool** `true` on success and `false` on failure

#### *protocol*FlashData()

Flash firmware to the chip from the memory buffer. The memory buffer must already contain the
data to send to the chip.

input: client **Stream ptr** an active Stream for output of status messages

return: **bool** `true` on success and `false` on failure

#### *protocol*DumpFlash()

Copy the chip firmware to the memory buffer. The operation will first copy the entire chip flash
to the memory buffer and then attempt to find the end of the program code and adjust the
size of the data in the memory buffer. The reported dump size will be in full page size increments
and will most often be slightly larger than the actual program code. The extra bytes will be
initialized to 0xFF.

input: client **Stream ptr** an active Stream for output of status messages

return: **bool** `true` on success and `false` on failure

#### *protocol*Erase()

Erase the chip.

input: client **Stream ptr** an active Stream for output of status messages

return: **bool** `true` on success and `false` on failure

--- */



#ifndef __CMDS_H__
#define __CMDS_H__

// -------- CMDS dispatch functions --------------------------------------------------------------------
bool cmdsInit();
void cmdsLoop();

void avrPowerOn();
void avrPowerOff();
bool avrPowerIsActive();
void avrDeviceSet(uint8_t dev_type);
bool cmdsProcessCommands(Stream *client, bool aborted);
void avrFlashFile(const char *name);


// -------- Program Interface function vectors --------------------------------------------------------------------
typedef struct {
	uint8_t dev_type;
	bool (*pgmrInit)();
	void (*pgmrLoop)();
	bool (*pgmrIsConnected)(Stream *client, bool silent);
	bool (*pgmrGetInfo)(Stream *client);
	bool (*pgmrReadFuses)(Stream *client);
	bool (*pgmrWriteFuse)(Stream *client, uint8_t fuse_number, uint8_t fuse_value);
	bool (*pgmrFlashData)(Stream *client, ihexStream *src);
	bool (*pgmrDumpFlash)(Stream *client, ihexStream *dest);
	bool (*pgmrErase)(Stream *client);
	bool (*pgmrWriteEeprom)(Stream *client, uint32_t offset, ihexStream *src);
	bool (*pgmrReadEeprom)(Stream *client, uint32_t offset, uint32_t size, ihexStream *dest);
} avrDevice;

#define AVR_DEVICE_NONE	0
#define AVR_DEVICE_SPI	1
#define AVR_DEVICE_UPDI	2
//#define AVR_DEVICE_SWD	3
//#define AVR_DEVICE_JTAG	4

#endif
