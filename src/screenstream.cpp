/* ***************************************************************************
* File:    screen.cpp
* Date:    2020.08.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------

### SCREEN API

Provides a class to the device

The basic navigation uses 3 buttons which provide up, down, and select.
The file list and menu are navigated with up/down and th center button performs selection of the current file or menu action.
The NAV also performs forward/back scrolling of the virtual screen buffer with the physical display acting as a viewport.
The center button returns the screen to the previous mode.

The display and UI interface supports text-line based display functions.

The use of various graphics - such as a progress bar - could be future enhancements.

There display has three modes:

- file list - a list of the files currently in SPIFFS storage
- file menu - list of options available for the currently selected file _(1)_
- text msg - a scrolling screen of messages for the current operations

**DEVELOPER NOTE:** the coordinate system has 0,0 in the upper left.

--- */

// now the local includes
#include "allincludes.h"

//#define SCREENDEBUG(fmt, ...)	DEBUGSERIAL.printf(fmt, ##__VA_ARGS__)
#ifndef SCREENDEBUG
#define SCREENDEBUG(fmt, ...) ((void)0)
#endif

#include "screenstream.h"

#define CUSTOM_COLOR(r, g, b)	  (((r & 0x1F) << 11) | ((g & 0x3F) << 5) | ((b & 0x1F))) // color is 5bits of red, 6 bits of green, and 5 bits of blue
#define COLOR_SCREEN_BACKGROUND	  TFT_BLACK
#define COLOR_HEADER_FOREGROUND	  TFT_WHITE
#define COLOR_HEADER_INFO		  TFT_YELLOW
#define COLOR_HEADER_BACKGROUND	  COLOR_SCREEN_BACKGROUND
#define COLOR_MENU_FOREGROUND	  0xFC9F
#define COLOR_MENU_BACKGROUND	  COLOR_SCREEN_BACKGROUND
#define COLOR_FILELIST_FOREGROUND TFT_SKYBLUE
#define COLOR_FILELIST_BACKGROUND COLOR_SCREEN_BACKGROUND
#define COLOR_MESSAGES_FOREGROUND TFT_DARKGREEN
#define COLOR_MESSAGES_BACKGROUND COLOR_SCREEN_BACKGROUND
#define COLOR_TRAFFIC_RX		  TFT_MAGENTA
#define COLOR_TRAFFIC_TX		  TFT_CYAN

#define ICON_AREA_W 39
#define ICON_AREA_H 14	// pixels
#define ICON_AREA_X (240 - ICON_AREA_W - 2) // assumes the right side of a 240 pixel wide display
#define ICON_AREA_Y 0

#define ICON_RADIUS (ICON_AREA_H / 2)

#define SCROLL_OFFSET 2 // number of lines from botton to start scrolling

#define DISPLAY_MODE_FILES	0
#define DISPLAY_MODE_TEXT	1
#define DISPLAY_MODE_SERIAL 2
#define DISPLAY_MODE_MENU	3
#define DISPLAY_MODE_HOME	DISPLAY_MODE_FILES

static const char *g_menu_entries_hex[] = {"`FLASH", "`DELETE", "`RETURN", NULL};
static const char *g_menu_entries_cmd[] = {"`RUN", "`LIST", "`DELETE", "`RETURN", NULL};
static const char *g_menu_entries_unkn[] = {"`DON'T DO IT", "`DELETE", "`RETURN", NULL};
#define OLED_FILE_MENU_FLASH  0
#define OLED_FILE_MENU_DELETE 1
#define OLED_FILE_MENU_RETURN 2

#define OLED_CMD_MENU_RUN    0
#define OLED_CMD_MENU_LIST   1
#define OLED_CMD_MENU_DELETE 2
#define OLED_CMD_MENU_RETURN 3

/* ---
#### icons()

update the icons immediately to their new state

state values are:

- DISPLAY_ICON_STATE_OFF: turn the icon off
- DISPLAY_ICON_STATE_ON: turn the icon on
- DISPLAY_ICON_STATE_UNK: do not change the icon

input:

- RX_state **int8_t** RX icon
- TX_state **int8_t** TX icon
- CON_state **int8_t** Remote client connected

NOTE: the icons are refreshed immediately using a partial refresh of just the icon area
--- */

void screenStream::refresh_battery() {
#if 1
	// display battery level in upper right corner
	const char *info = ioGetBatteryInfo();
	uint16_t x_pos = (this->display->width() - this->display->textWidth(info));
	this->display->setTextColor(COLOR_HEADER_INFO, COLOR_HEADER_BACKGROUND);
	this->display->setTextSize(1);
	this->display->setTextFont(WINDOW_FONT);
	this->display->fillRect(x_pos, 6, this->display->width(), FONT_HEIGHT, COLOR_HEADER_BACKGROUND);
	this->display->setCursor(x_pos, 6); // Y pos is set to different in height between header font and regular font
	this->display->print(info);
#endif
}

uint16_t screenStream::refresh_header(const char *title) {
	uint16_t y_pos, x_pos;

	this->display->fillScreen(COLOR_SCREEN_BACKGROUND);

	// text positioning uses the top left of the font box

	// render header title area

	// menu title centered on top line
	this->display->setTextColor(COLOR_HEADER_FOREGROUND, COLOR_HEADER_BACKGROUND);
	this->display->setTextSize(1);
	this->display->setTextFont(HEADER_FONT);
	x_pos = (this->display->width() - this->display->textWidth(title)) / 2;
	y_pos = 0;
	this->display->setCursor(x_pos, y_pos);
	this->display->print(title);

	x_pos = 0;
	y_pos = this->display->fontHeight() + 1;
	this->display->drawFastHLine(0, y_pos, this->display->width(), TFT_WHITE);
	this->display->setTextFont(WINDOW_FONT);
	this->display->setTextSize(1);
	y_pos += 4; // position of next line of text

	this->refresh_battery();

	return (y_pos);
}

void screenStream::refresh_current() {
	const char *title = "Files";

	uint16_t x_pos = 0, y_pos = 0;

	// recompute any list data we may need
	if (this->display_mode == DISPLAY_MODE_MENU) {
		title = this->selected_filename;
		if (title[0] && (title[0] == '/'))
			title++; // skip the leading forward slash
		refresh_menu();
	}
	else
		refresh_file_list();

	// render header title are		a
	y_pos = this->refresh_header(title);

	// render file list with simulated scrolling

	for (int8_t i = 0; i < VIEWPORT_LIST_LINES; i++) {
		if (this->lines_buffer[i][0]) {
			char *line = this->lines_buffer[i];
			x_pos = (1 * FONT_WIDTH);
			// look for our secret 'center me' character
			if (*line == '`') {
				line++;	// skip the character
				// adjust the starting point to center the text
				x_pos = (this->display->width() - this->display->textWidth(line)) / 2;
			}

			this->display->setCursor(x_pos, y_pos); // indent 1 character
			this->display->fillRect(x_pos, y_pos, this->display->width(), FONT_HEIGHT, COLOR_MENU_BACKGROUND);
			this->display->setTextColor(COLOR_FILELIST_FOREGROUND, COLOR_FILELIST_BACKGROUND);

			if (i == (this->file_list_index - this->skipped_lines)) {
				// invert the active row
				this->display->fillRect(0, y_pos - 1, this->display->width(), FONT_HEIGHT, COLOR_FILELIST_FOREGROUND);
				this->display->setTextColor(COLOR_FILELIST_BACKGROUND, COLOR_FILELIST_FOREGROUND);
			}
			this->display->print(line);
			y_pos += FONT_HEIGHT;
		}
	}
	this->icons(DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_UNK); // repaint icons with their current state
	this->display_dirty = false;

}

void screenStream::refresh_file_list() {
	// TODO now that we have a virtual screen, we should load up all of the possible filenames and then scroll that list
	{
		char *filename = 0;
		uint16_t line_index = 0;

		this->file_list_length = 0;
		this->skipped_lines = 0;

		filename = filesysGetFileInfo(true, false);
		for (int8_t i = this->file_list_index - (VIEWPORT_LIST_LINES - SCROLL_OFFSET); i >= 0; i--) {
			filename = filesysGetFileInfo(false, false);
			if (!filename)
				break;
			SCREENDEBUG("skipping %s\n", filename);
			this->skipped_lines++;
			this->file_list_length++;
		}
		VERBOSE("max list size = %d list index = %d  skipped lines = %d\n", VIEWPORT_LIST_LINES, this->file_list_index, this->skipped_lines);

		// now we copy the visible list of files into the display buffer
		for (int8_t i = 0; i < VIEWPORT_LIST_LINES; i++) {
			this->lines_buffer[line_index][0] = 0;
			if (filename) {
				this->file_list_length++;

				// if there is a tab then we want to split the string into the name and the size info
				char *pos = strstr(filename, "\t");
				if (pos) {
					*pos++ = 0;
					snprintf(this->lines_buffer[line_index], VIEWPORT_LINE_CHARS, "%s %*s", filename, (((VIEWPORT_LINE_CHARS - 1) - strlen(filename)) - 2), pos);
				} else
					strncpy(this->lines_buffer[line_index], filename, VIEWPORT_LINE_CHARS);
				this->lines_buffer[line_index][VIEWPORT_LINE_CHARS - 1] = 0;
				filename = filesysGetFileInfo(false, false);
			}
			line_index++;
		}

		// finish counting the files not visible so it is available for the button processing
		while (filename) {
			this->file_list_length++;
			filename = filesysGetFileInfo(false, false);
		}
	}
}

void screenStream::refresh_menu() {
	for (int8_t i = 0; i < VIEWPORT_LIST_LINES; i++)
		this->lines_buffer[i][0] = 0;
	this->file_list_length = 0;
	this->skipped_lines = 0;

	const char **menu;
	if (this->selected_type == FILE_TYPE_HEX)
		menu = g_menu_entries_hex;
	else if (this->selected_type == FILE_TYPE_CMD)
		menu = g_menu_entries_cmd;
	else {
		//MESSAGE("Unknown file type - %s\n", this->filename);
		menu = g_menu_entries_unkn;
	}
		for (this->file_list_length = 0; menu[this->file_list_length] != NULL; this->file_list_length++)
			strncpy(this->lines_buffer[this->file_list_length], menu[this->file_list_length], VIEWPORT_LINE_CHARS);
}

void screenStream::refresh_text_mode(bool optimize) {
	uint16_t x_pos, y_pos;

	// TODO need to optimize for use case where text added to line but not wrapped

	this->display->setTextFont(WINDOW_FONT);
	this->display->setTextSize(1);
	this->display->setTextColor(COLOR_MESSAGES_FOREGROUND, COLOR_MESSAGES_BACKGROUND);
	y_pos = 0;
	int16_t i = 0;

	if (optimize) {
		i = this->current_display_line - this->current_viewport_first_line;
		y_pos = (this->current_display_line - this->current_viewport_first_line) * FONT_HEIGHT;
	}
	else {
		//this->display->fillScreen(COLOR_SCREEN_BACKGROUND);	// causes a bit of flicker but insures teh display is rendered correctly
	}

	for (; i < VIEWPORT_LINES; i++) {
		// presumably the viewport tracking variables have been bound to not exceed the buffers but let's just make sure
		if (this->current_viewport_first_line > (SCREEN_LINES - VIEWPORT_LINES))
			this->current_viewport_first_line = (SCREEN_LINES - VIEWPORT_LINES);
		if (this->current_viewport_first_line_char > (SCREEN_LINE_CHARS - VIEWPORT_LINE_CHARS))
			this->current_viewport_first_line_char = (SCREEN_LINE_CHARS - VIEWPORT_LINE_CHARS);

		// check if the line has any content
		if (this->lines_buffer[this->current_viewport_first_line+i][this->current_viewport_first_line_char]) {
			// to save on clipping effort, we temporarily truncate the line to the width of the viewport
			char *ptr = &(this->lines_buffer[this->current_viewport_first_line + i][this->current_viewport_first_line_char + VIEWPORT_LINE_CHARS]);
			uint8_t hold = *ptr;
			*ptr = 0;
			ptr -= VIEWPORT_LINE_CHARS;
			x_pos = 0;
			char *segment = ptr;

			// look for our secret 'center me' character
			if (*segment == '`') {
				segment++;	// skip the character
				// adjust the starting point to center the text
				x_pos = (this->display->width() - this->display->textWidth(segment)) / 2;
				SCREENDEBUG("center line(%s) starting at %d\n", segment, x_pos);
			}

			this->display->fillRect(0, y_pos, this->display->width(), FONT_HEIGHT, COLOR_MENU_BACKGROUND);
			this->display->setCursor(x_pos, y_pos);
			this->display->print(segment);

			SCREENDEBUG("[%03d,%03d](%2d): [%s]\n", x_pos, y_pos, strlen(segment), segment);
			ptr += VIEWPORT_LINE_CHARS;
			*ptr= hold;
		}
		else {
			// clear blank lines
			this->display->fillRect(0, y_pos, this->display->width(), FONT_HEIGHT, COLOR_MENU_BACKGROUND);
		}

		y_pos += FONT_HEIGHT;
	}
	SCREENDEBUG("\n");

	this->icons(DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_UNK); // repaint icons with their current state
}


void screenStream::updateListPosition(int amount) {
	VERBOSE("screenStream::updateListPosition(%d)\n", amount);
	this->file_list_index += amount;
	if (amount > 0) {
		if (this->file_list_index > ((this->file_list_length - SCROLL_OFFSET) + 1))
			this->file_list_index = 0;
	}
	else {
		if (this->file_list_index < 0)
			this->file_list_index = (this->file_list_length - SCROLL_OFFSET) + 1;
			if (this->file_list_index < 0)
				this->file_list_index = 0;
	}
	this->display_dirty = true;
}

bool screenStream::isTextMode() {
	VERBOSE("screenStream::isTextMode() is %s (mode=%d)\n", ((this->display_mode == DISPLAY_MODE_TEXT) ? "true" : "false"),  this->display_mode);
	return (!((this->display_mode == DISPLAY_MODE_FILES) || (this->display_mode == DISPLAY_MODE_MENU)));
}


void screenStream::scrollUp() {
	if (this->current_viewport_first_line > 0) {
		this->current_viewport_first_line--;
		this->refresh_text_mode(false);
		this->display_dirty = true;
	}
}

void screenStream::scrollDown() {
	if ((this->current_viewport_first_line < (SCREEN_LINES - VIEWPORT_LINES)) &&
		((this->current_viewport_first_line + VIEWPORT_LINES) < this->current_display_line))
	{
		this->current_viewport_first_line++;
		this->refresh_text_mode(false);
		this->display_dirty = true;
	}
}

void screenStream::scrollLeft() {
	if (this->current_viewport_first_line_char > 0) {
		this->current_viewport_first_line_char--;
		this->refresh_text_mode(false);
		this->display_dirty = true;
	}
}

void screenStream::scrollRight() {
	if (this->current_viewport_first_line_char < (SCREEN_LINE_CHARS - VIEWPORT_LINE_CHARS)) {
		this->current_viewport_first_line_char++;
		this->refresh_text_mode(false);
		this->display_dirty = true;
	}
}


void screenStream::selectCurrent() {
	if (this->display_mode == DISPLAY_MODE_FILES) {
		// the currently highlighted entry is (this->file_list_index or (this->file_list_index - (VIEWPORT_LIST_LINES - SCROLL_OFFSET)))
		int index = this->file_list_index - this->skipped_lines;

		if (index < 0) {
			 // insanity check ... how the heck did this happen ?!
			VERBOSE("woops, selected #%d\n", index);
			 index = 0;
		}

		// to be honest, this code is getting pretty convoluted with needing knowledge of filesys and avr
		// a bit kludgy but we assume filenames do not have spaces

		if (index >= VIEWPORT_LINES) { // insanity check
			memcpy(this->selected_filename, "bad.file", MAX_FILENAME_LEN);
		}
		else if (this->lines_buffer[index][0] == 0) {
			// no file so we want to abort
			return;
		} else {
			memcpy(this->selected_filename, this->lines_buffer[index], MAX_FILENAME_LEN);
			this->selected_filename[MAX_FILENAME_LEN] = 0;
		}

		// the file list has names and sizes; we only want the name
		// we assume no spaces in names so first space is end of name
		char *pos = strstr(this->selected_filename, " ");
		if (pos)
			*pos = 0;
		this->selected_type = filesysGetType(this->selected_filename);
		//this->file_list_index = 0;
		this->display_dirty = true;
		this->file_list_index = 0;
		this->display_mode = DISPLAY_MODE_MENU;
		VERBOSE("selected #%d %s\n", index, this->selected_filename);
	} else {
		// DISPLAY_MODE_MENU
		if (this->selected_type == FILE_TYPE_CMD) {
			switch (this->file_list_index) {
				case OLED_CMD_MENU_RUN: {
					if (this->selected_type == FILE_TYPE_CMD)
						ioRunCommandFile(this->selected_filename, false);
					else {
						// bad file type so we give up and return.
						VERBOSE("Unknown file type - this->selected_filename\n");
						this->clear(true);
					}
				} break;
				case OLED_CMD_MENU_LIST: {
					ioRunCommandPrintf("CAT %s", this->selected_filename);
				} break;
				case OLED_CMD_MENU_DELETE: {
					ioRunCommandPrintf("DEL %s", this->selected_filename);
				} // fall through
				case OLED_CMD_MENU_RETURN: {
					this->clear(true);
				} break;
				default: {
					MESSAGE("menu unknown[%d] action on %s\n", this->file_list_index, this->selected_filename);
				} break;
			}
		}
		else {
			switch (this->file_list_index) {
				case OLED_FILE_MENU_FLASH: {
					if (this->selected_type == FILE_TYPE_HEX)
						ioRunCommandPrintf("LOAD %s WRITE", this->selected_filename);
					else {
						// bad file type so we give up and return.
						VERBOSE("Unknown file type - this->selected_filename\n");
						this->clear(true);
					}
				} break;
				case OLED_FILE_MENU_DELETE: {
					ioRunCommandPrintf("DEL %s", this->selected_filename);
				} // fall through
				case OLED_FILE_MENU_RETURN: {
					this->clear(true);
				} break;
				default: {
					MESSAGE("menu unknown[%d] action on %s\n", this->file_list_index, this->selected_filename);
				} break;
			}
		}
	}
}

bool screenStream::timeout(int seconds) {
	static uint32_t idle_timeout = 0;

	if (seconds) {
		VERBOSE("Updating Idle Timeout to %d seconds\n", seconds);
		idle_timeout = millis() + (seconds * 1000);
		//this->text_mode_delay = millis() + (IDLE_ACTION_TIMEOUT * 1000);	// delay a possible switch to text mode
		return false;
	}
	if (idle_timeout > millis())
		return false;

	return true;
}

void screenStream::delayTextMode(bool update) {
#if 0	// this was to allow time for the menu system to be used but (1) it also caused the menu system to get stuck, and (2) text mode abuse is the user's fault and the PortaProg shouldn't try to fix bad user behavior
	if (update) {
		this->text_mode_delay = millis() + (IDLE_ACTION_TIMEOUT * 1000);
		VERBOSE("Updating Text Mode Timeout: %d\n", this->text_mode_delay);
	}
	else {
		this->text_mode_delay = 0;
		VERBOSE("Clearing Text Mode Timeout\n");
	}
#else
		this->text_mode_delay = 0;
		VERBOSE("Text Mode Delay has been redacted\n");
#endif
}

/* ---
#### screenStream::clear()

delete all current content and mark display buffer as dirty so it is refreshed during the next cycle

input: **bool** reset to _HOME_ mode
--- */
void screenStream::clearData() {
	VERBOSE("screenStream::clearData()\n");
	// if we have done a FLASH operation, then don't reset the display; we revert to MENU mode
		for (int i = 0; i < SCREEN_LINES; i++)
			memset(this->lines_buffer[i], 0, SCREEN_LINE_CHARS + 1);
		this->current_display_line = 0;
		this->current_display_line_char = 0;
		this->current_viewport_first_line = 0;
		this->current_viewport_first_line_char = 0;
		this->file_list_index = 0;
		this->file_list_length = 0;
		this->skipped_lines = 0;
}

void screenStream::clear(bool home) {
	VERBOSE("screenStream::clear(%d) mode=%d\n", home, this->display_mode);
	this->clearData();
	if (home)
		this->display_wrap = true;

	if (home)
		this->display_mode = DISPLAY_MODE_HOME;

	this->delayTextMode(false);				// allow immediate text mode
	this->display->fillScreen(COLOR_SCREEN_BACKGROUND);
	this->display_dirty = true;
}

/* ---
#### screenStream::wrapOff()

turn off text wrap for text mode
--- */
void screenStream::wrapOff() {
	this->display_wrap = false;
}

/* ---
#### screenStream::wrapOn()

turn on text wrap for text mode (default)
--- */
void screenStream::wrapOn() {
	this->display_wrap = true;
}

/* ---
#### screenStream::displayOff()

turn off the display and power down the back light
--- */
void screenStream::displayOff() {
	VERBOSE("screenStream::displayOff()\n");
#ifdef PIN_DISP_LIGHT
	digitalWrite(PIN_DISP_LIGHT, 0);
#endif
	this->display->writecommand(TFT_DISPOFF);
	this->display->writecommand(TFT_SLPIN);
}

/* ---
#### screenStream::displayOn()

turn on the display and power up the back light
--- */
void screenStream::displayOn() {
	VERBOSE("screenStream::displayOn()\n");
	this->display->writecommand(TFT_DISPON);
	this->display->writecommand(TFT_SLPOUT);
}

/* ---
#### screenStream::refresh()

repaint the display if is has been marked _dirty_ explicitly or by other operations

The display will automatically revert to the file list after a 90 second timeout without
updates or user interaction.
--- */
void screenStream::refresh() {
	// provide and idle timeout to go 'home'

	// three possible display modes: SPIFFS file list; FLASH Progress; Serial output
	// eventually we will have different things to refresh
	if (this->display_dirty) {
		VERBOSE("screenStream::refresh() dirty\n");
		if (this->display_mode == DISPLAY_MODE_FILES) {
			this->refresh_current();
		} else if (this->display_mode == DISPLAY_MODE_MENU) {
			this->refresh_current();
		}
		this->display_dirty = false;
	} else {
		// if we are not on the home screen but have been idle for too long, we automatically revert
		if (this->timeout(IDLE_TIMEOUT_TEST)) {
			if (this->display_mode == DISPLAY_MODE_TEXT) {
				MESSAGE("\n");
				VERBOSE("Display timout. Going previous screen.\n");
				this->clear(true);
			}
			else {
				VERBOSE("Periodic screen refresh.\n");
				// we really only need to update the battery level
				this->refresh_battery();
			}
			this->timeout(IDLE_USER_TIMEOUT);	// reset timer
		}
	}
}

void screenStream::icons(int8_t rx, int8_t tx, int8_t con) {
	static bool rx_icon = false, tx_icon = false, client_icon = false;
	bool dirty = false;
	uint16_t cx = 0, cy = 0, cd = 0; // circle center x, y, and diameter


	if (rx != DISPLAY_ICON_STATE_UNK) {
		if (rx_icon != rx)
			dirty = true;
		rx_icon = rx;
	}
	if (tx != DISPLAY_ICON_STATE_UNK) {
		if (tx_icon != tx)
			dirty = true;
		tx_icon = tx;
	}
	if (con != DISPLAY_ICON_STATE_UNK) {
		if (client_icon != con)
			dirty = true; // if we ignore this case, then the client connectivity only refreshes on the next change of RX or TX
		client_icon = con;
	}

	if (!dirty)
		return;

	// erase icon area

	this->display->drawRoundRect(ICON_AREA_X, ICON_AREA_Y, ICON_AREA_W, ICON_AREA_H, ICON_AREA_H / 2, COLOR_HEADER_BACKGROUND);

	// draw client oval - solid if client connected, else hollow
	cx = ICON_AREA_X + ICON_RADIUS;
	cy = ICON_AREA_Y + ICON_RADIUS;
	cd = ICON_RADIUS;

	if (client_icon) {
		// filled oval is half disc at each end and box in the middle
		this->display->fillRoundRect(ICON_AREA_X, ICON_AREA_Y, ICON_AREA_W, ICON_AREA_H, ICON_AREA_H / 2, COLOR_HEADER_FOREGROUND);
	} else {
		cx = ICON_AREA_X + ICON_RADIUS;
		// filled oval is half disc at each end and box in the middle
		this->display->drawRoundRect(ICON_AREA_X, ICON_AREA_Y, ICON_AREA_W, ICON_AREA_H, ICON_AREA_H / 2, COLOR_HEADER_FOREGROUND);
	}
	//display->setDrawColor(2); // XOR

	cx = ICON_AREA_X + ICON_RADIUS;
	cy = ICON_AREA_Y + ICON_RADIUS;
	cd = ICON_RADIUS - 2;

	// draw rx circle - solid if active, hollow if not; drawn XOR with background
	if (tx_icon) {
		// filled circle
		this->display->fillCircle(cx, cy, cd, COLOR_TRAFFIC_TX);
	} else {
		// hollow circle
		//display->drawCircle(cx, cy, cd, U8G2_DRAW_ALL);
	}

	// draw tx circle - solid if active, hollow if not; drawn XOR with background
	cx = (ICON_AREA_X + ICON_AREA_W) - ICON_RADIUS;
	if (rx_icon) {
		// filled circle
		this->display->fillCircle(cx, cy, cd, COLOR_TRAFFIC_RX);
	} else {
		// hollow circle
		//display->drawCircle(cx, cy, cd, U8G2_DRAW_ALL);
	}
}



// -------------------------------------------------------------------------
// ------------------------ Public Methods ---------------------------------
// -------------------------------------------------------------------------


/* ---
#### screenStream::screenStream()

Implements a compatible stream class where print() and write() operates on the device display.

--- */

screenStream::screenStream(uint32_t buffer_size) {
	VERBOSE("screenStream::screenStream\n");

	this->display = new TFT_eSPI();

	if (this->display != NULL) {
	this->display->init();
	this->displayOn();
	this->display->setRotation(1); // use the display wider than tall
	this->display->setTextFont(WINDOW_FONT);
	this->display->setTextSize(1);
	this->display->setTextWrap(false);
	}
	else {
		VERBOSE("screenStream no display driver\n");
	}

	this->selected_filename[0] = 0;
	this->selected_type = FILE_TYPE_UNKN;

	this->clear(true);
	this->icons(DISPLAY_ICON_STATE_OFF, DISPLAY_ICON_STATE_OFF, DISPLAY_ICON_STATE_OFF);
	this->delayTextMode(false);				// allow immediate text mode
	this->display_mode = DISPLAY_MODE_TEXT; // we start in text mode so startup messages are displayed immediately
}

screenStream::~screenStream() {
}


size_t screenStream::write(uint8_t b) {
	char tiny_buffer[2];
	// need to send this byte to the display
	// this is very inefficient; TODO determine where this is called from
	tiny_buffer[0] = b;
	tiny_buffer[1] = 0;
	this->print(tiny_buffer);
	return 1;
}

int screenStream::print(const char *line) {
	//output the message to serial by default
	VERBOSE("screenStream::print(%s)\n", line);

	if (this->display_mode != DISPLAY_MODE_TEXT) {
		this->clearData();
		this->display_mode = DISPLAY_MODE_TEXT;
	}

	char *p = (char *)line;
	int count = 0;
	int most_recent_line = this->current_display_line;

	// this is a bit of a kludge but I've decided there is no reason to wrap IHEX lines
	bool allow_wrapping = true;
	if ((line && (line[0] == ':')) || (!this->display_wrap)) {
		allow_wrapping = false;
	}

	// for the various 'print' methods, we wrap long lines, wrap at a newline, and trim internal whitespace
	int wrapping_mode = 0;	// 0=not_wrapping, 1=newline_wrapping, 2=end_of_line_buffer_wrapping
	bool padded = false;

	while (*p) {
		SCREENDEBUG("%02X ", *p);
		// if we hit a linefeed then ignore it
		if (*p == '\r') {
			p++;
			VERBOSE("skipping linefeed\n");
			continue;
		}
		if (*p == '`') {
			VERBOSE("FYI: we will center [%s]\n", p);
		}
		if (*p == '\n') {
			this->lines_buffer[this->current_display_line][this->current_display_line_char] = 0;
			this->current_display_line++;
			this->current_display_line_char = 0;
			this->current_viewport_first_line_char = 0;
			wrapping_mode = 1;
			SCREENDEBUG("newline line wrap\n");
		} else if ((this->current_display_line_char >= SCREEN_LINE_CHARS) || (*p == '`')) {	// force wrap or our super secret 'center on line' character
			if (allow_wrapping) {
				this->lines_buffer[this->current_display_line][SCREEN_LINE_CHARS] = 0;
				this->current_display_line++;
				this->current_display_line_char = 0;
				this->current_viewport_first_line_char = 0;
				wrapping_mode = 2;
				SCREENDEBUG("computed line wrap\n");
			}
			else {
				wrapping_mode = 0;
				SCREENDEBUG("preventing line wrap\n");
				if (*p) {
					p++; // throw the character away
				}
				continue;
			}
		}
		if (*p == '\t')
			*p = ' ';

		// if we wrapped and wrapping lines is allowed, then store the character and we keep going
		if (wrapping_mode) {
			SCREENDEBUG("wrapping mode = %d\n", wrapping_mode);
			// if there are no more lines in our screen buffer, we shift it by 1 line to make room
			if (this->current_display_line >= SCREEN_LINES) {
				VERBOSE("need to scroll the line buffer - line %d\n", this->current_display_line);
				// scroll to make room
				for (int i = 0; i < (SCREEN_LINES - 1); i++) {
					memcpy(this->lines_buffer[i], this->lines_buffer[i + 1], SCREEN_LINE_CHARS);
					SCREENDEBUG("[%02d] %s\n", i, this->lines_buffer[i]);
				}

				// set current line to the last line in the buffer
				this->current_display_line = (SCREEN_LINES - 1);
				most_recent_line = 0;	// need to invalidate optimization since we scrolled to reuse the last line

				// clear the last line buffer
				memset(this->lines_buffer[this->current_display_line], 0, SCREEN_LINE_CHARS);
				//this->lines_buffer[this->current_display_line][SCREEN_LINE_CHARS] = 0;
				SCREENDEBUG("[%02d] %s\n", this->current_display_line, this->lines_buffer[this->current_display_line]);
			}
			count++;	// we consider wrapping as a byte to be counted
		}

		if ((*p) && (wrapping_mode != 1)) {
			// we have a character and it's not the newline we probably need to output it on the small displays,
			// we dont need to output spaces at the start of a line
#if (VIEWPORT_LINE_CHARS < 30)
			if (!((this->current_display_line_char == 0) && (*p == ' '))) {
				if ((!padded) || (*p != ' ')) {
					this->lines_buffer[this->current_display_line][this->current_display_line_char] = *p;
					this->current_display_line_char++;
					count++;
					// on the small OLED display, we filter out padded spaces
					if (*p == ' ')
						padded = true;
					else
						padded = false;
				}
				else {
					SCREENDEBUG("skipping extra whitespace\n");
				}
			}
			else {
				SCREENDEBUG("skipping leading whitespace\n");
			}
#else
			this->lines_buffer[this->current_display_line][this->current_display_line_char] = *p;
			this->current_display_line_char++;
			count++;
#endif
		}
		else {
			SCREENDEBUG("skipping newline\n");
		}
		wrapping_mode = 0;

		if ((this->current_display_line - this->current_viewport_first_line) >= VIEWPORT_LINES) {
			// if the latest display will render off the bottom of the viewport, we shift the viewport
			if (this->current_viewport_first_line < (SCREEN_LINES - VIEWPORT_LINES)) {
				SCREENDEBUG("scrolling viewport\n");
				this->current_viewport_first_line++;
			}
		}
		else if ((this->current_display_line_char - this->current_viewport_first_line_char) >= VIEWPORT_LINE_CHARS) {
			// if the latest display will render off the right of the viewport, we shift the viewport
			if (this->current_viewport_first_line_char < (SCREEN_LINE_CHARS - VIEWPORT_LINE_CHARS)) {
				this->current_viewport_first_line_char++;
			}
		}

		if (*p) {
			p++; // advance to next character
		}
	} // end while

	SCREENDEBUG("\n");

	// manually refresh, because other operations or the UI may be blocking
	// if the original recent_line is the same as the current line, then we can optimize the refresh)
	if (this->text_mode_delay != 0) {
		if (this->text_mode_delay < millis()) {
			this->delayTextMode(false);	// clear out the delay
			this->display->fillScreen(COLOR_SCREEN_BACKGROUND);	// we dont want to clear all teh data, just the display
			this->display_mode = DISPLAY_MODE_TEXT;
			most_recent_line = -1;	// force screen repaint
		}
	}
	if ((this->text_mode_delay == 0) && this->display_mode == DISPLAY_MODE_TEXT) {
		this->display_dirty = true;
		this->timeout(IDLE_USER_TIMEOUT);
		this->refresh_text_mode((most_recent_line == this->current_display_line));
	}
	else {
		VERBOSE("Defering refresh: delay = %d, mode = %d\n", this->text_mode_delay, this->display_mode);
	}

	return count;
}

int screenStream::printf(const char *fmt, ...) {
	static char line[MAX_LINE_TEXT + 1];
	va_list args;
	va_start(args, fmt);
	line[0] = 0;
	vsnprintf(line, MAX_LINE_TEXT, fmt, args);
	line[MAX_LINE_TEXT - 1] = 0;
	va_end(args);

	return this->print(line);
}

int screenStream::availableForWrite() {
	return MAX_LINE_TEXT;	// this is the size of rendering buffer
}

void screenStream::flush() {
	//I'm not sure what to do here...
}

