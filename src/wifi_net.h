#ifndef _WIFINET_H
#define _WIFINET_H

#define TCP_PORT 		8888	// could be anything for starters adn the user can override it with the .config file
#define TELNET_PORT 	23		// its default

#define MAX_NETWORK_TEXT 256
extern char g_network_buf[];	// it is shared between the telnet and the tcp processing
//extern WiFiClient g_tcp_client;	// I hate the notion but things are not working with attempting to pass by reference

bool wifiInit();
void wifiLoop();

bool otaInit();
void otaLoop();

bool wifiIsConnected();
bool wifiConnectionLost();
bool wifiIsHotspot();

char *wifiAddress();
uint8_t wifiAddressEnding();

void wifiOff();
void wifiOn();

#endif
