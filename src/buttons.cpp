/* ***************************************************************************
* File:    buttons.cpp
* Date:    2020.08.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------

### BUTTONS API

Provides the button procssessing for the device UI.

The API is configurable to support the display and button(s) of either the `TTGO18650` or the `TTGOT18`.

The `TTGO18650` purposes the BOOT button for a 1-button interface.

The 1-button UI uses short press to move to the next line of the list or menu.
At the end of the list or menu, the next button press will loop back to the start.
A long press selects the current file or menu action.
When displaying text, a single press returns the screen to the previous mode.

The `TTGOT18` has 5 buttons which act like a pseudo joystick with up, down, left, right, and center (select).

--- */

#include "allincludes.h"
#include "buttons.h"

#define LONG_PRESS_DURATION 750 // milliseconds

static uint8_t g_button_states[BUTTON_MAX];

bool buttonsInit() {
	for (uint8_t i = 0; i < BUTTON_MAX; i++) {
		pinMode(g_button_pins[i], INPUT_PULLUP);
		g_button_states[i] = 0;
	}
	return true;
}

uint8_t buttonGetState(uint8_t button_index) {
	DEBUG("Button[%d] = %d\n", button_index, g_button_states[button_index]);
	return g_button_states[button_index];
}

bool buttonIsActive(uint8_t button_index) {
	DEBUG("Button[%d] = %d\n", button_index, g_button_states[button_index]);
	return ((g_button_states[button_index] == BUTTON_LONG_PRESS) || (g_button_states[button_index] == BUTTON_PRESS));
}

uint8_t buttonSetState(uint8_t button_index, uint8_t state) {
	return (g_button_states[button_index] = state);
}

/* ---
#### buttonsUpdate()()

*Internal function* Return the current button state

The range of button states allows the caller to track both the start of a press event or long press as well as the eventual release.

 The button states are:

- IDLE - the button has no interaction
- PRESS - the button has been pressed
- LONG PRESS - the button has been pressed and held down for more than 750 ms
- RELEASE - the button has been released after being pressed
- LONG RELEASE - the button has been released after being held for more than 750 ms
- UNKNOWN - something went wrong ... probably because the programmer of this code didn't have enough espresso and was asleep at the wheel

**Note**: When this function is called from multiple places, the status will reflect the change between calls, regardless of caller.  *(this is a polling function, not interrupt driven)*

 return: **bool** true if any buttons have changed state
--- */

bool buttonsUpdate() {
	// it appears the 5-switch pseudo joystick may have a bounce issue or just be cheap as @$%&^; so we do a little Sw debounce

	static unsigned long time_pressed[BUTTON_MAX]; // we are assuming the data is initialized to 0's when powered up
	bool dirty = false;

	for (uint8_t i = 0; i < BUTTON_MAX; i++) {
		// button input is PULLUP, a low reading means it is pressed, a high reading means it is not
		if (!digitalRead(g_button_pins[i])) {
			switch (g_button_states[i]) {
				case BUTTON_IDLE: {
					g_button_states[i] = BUTTON_PRESS;
					time_pressed[i] = millis();
					VERBOSE("PRESS #%d\n", i);
					dirty = true;
				} break;
				case BUTTON_PRESS: {
					if ((time_pressed[i] + LONG_PRESS_DURATION) < millis()) {
						g_button_states[i] = BUTTON_LONG_PRESS;
						VERBOSE("LONG PRESS #%d\n", i);
						dirty = true;
					}
				} break;
				default: {
					//current_state = BUTTON_UNKNOWN;
				} break;
			}
		} else {
			switch (g_button_states[i]) {
				case BUTTON_PRESS: {
					g_button_states[i] = BUTTON_RELEASE;
					time_pressed[i] = 0;
					VERBOSE("RELEASE #%d\n", i);
					dirty = true;
				} break;
				case BUTTON_LONG_PRESS: {
					g_button_states[i] = BUTTON_LONG_RELEASE;
					time_pressed[i] = 0;
					VERBOSE("LONG RELEASE #%d\n", i);
					dirty = true;
				} break;
				case BUTTON_RELEASE:
				case BUTTON_LONG_RELEASE: {
					g_button_states[i] = BUTTON_IDLE;
					VERBOSE("IDLE #%d\n", i);
					//dirty = true;
				} break;
				default: {
					//current_state = BUTTON_UNKNOWN;
				} break;
			}
		}
	}

	if (dirty) {
		VERBOSE("New Button State C%d U%d D%d X%d Y%d Z%d\n", g_button_states[BUTTON_CENTER], g_button_states[BUTTON_UP], g_button_states[BUTTON_DOWN], g_button_states[BUTTON_EX1], g_button_states[BUTTON_EX2], g_button_states[BUTTON_EX3]);
		//Serial.printf("New Button State C%d U%d D%d X%d Y%d Z%d\n", g_button_states[BUTTON_CENTER], g_button_states[BUTTON_UP], g_button_states[BUTTON_DOWN], g_button_states[BUTTON_EX1], g_button_states[BUTTON_EX2], g_button_states[BUTTON_EX3]);
	}

	return dirty;
}
