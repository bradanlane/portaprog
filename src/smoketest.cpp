/* ***************************************************************************
* File:    smoketest.cpp
* Date:    2020.08.09
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------
### SMOKETEST

Perform hardware level testing of an assembled PortaProg

The test sequences all the pins of the header (using an LED block) and reports button presses

--- */



// local includes
#include "allincludes.h"

const char *_test_pin_names[] = {"MISO", "CLK", "MOSI", "RESET", "sGND", "SDA", "SCL", "TX", "RX", "UPDI"};
const char *_test_button_names[] = {"NAV CENTER", "NAV LEFT", "NAV RIGHT", "ACTION1", "ACTION2", "ACTION3"};
const char *_test_button_states[] = {"IDLE", "PRESSED", "LONGPRESSED", "RELEASED", "LONGRELEASED"};

uint8_t _test_pin_set[] = {PIN_SPI_MISO, PIN_SPI_CLK, PIN_SPI_MOSI, PIN_SPI_RESET, PIN_PWR_SW, PIN_I2C_SDA, PIN_I2C_SCL, PIN_UART2_TX, PIN_UART2_RX, PIN_UPDI_RX};
uint8_t _test_pin_index;
uint16_t _test_buttons;
uint32_t _test_timer;
#define TEST_INTERVAL 1000

void _test_pin() {
	digitalWrite(_test_pin_set[_test_pin_index], LOW);
	_test_pin_index++;

	if (_test_pin_index >= sizeof(_test_pin_set))
		_test_pin_index = 0;

	digitalWrite(_test_pin_set[_test_pin_index], HIGH);
	MESSAGE("TEST %s\n", _test_pin_names[_test_pin_index]);
}

bool testInit() {
	for (int i = 0; i < sizeof(_test_pin_set); i++) {
		pinMode(_test_pin_set[i], OUTPUT);
		digitalWrite(_test_pin_set[_test_pin_index], LOW);
	}

	_test_pin_index = 0;
	_test_buttons = 0;
	_test_timer = millis();
	return true;
}

void testLoop() {
	if (millis() > _test_timer) {
		_test_timer += TEST_INTERVAL;
		_test_pin();
	}
	if (buttonsUpdate()) {
		for (int i = 0; i < BUTTON_MAX; i++) {
			int state;
			if ((state = buttonGetState(i)))
				MESSAGE("%s %s\n", _test_button_names[i], _test_button_states[state]);
		}
	}
}
