// we created this @#^@#$^ file because the order of includes has become very strict. #$%^#$%^ !!!!

#include <Arduino.h>
#include <functional>

#include <FS.h>
#include <SPIFFS.h>

// ----------------------------------------------------------------------------
// -- useful macros for message display at various levels of importance -------
// ----------------------------------------------------------------------------

#define DEBUGSERIAL	Serial	// used for messages while debugging the PortaProg code
#define CHIPSERIAL	Serial1	// used to communicate with an attached device
#define UPDISERIAL	Serial2	// used for the UPDI interface for programming chips

//#define DEBUG(fmt, ...)		DEBUGSERIAL.printf(fmt, ##__VA_ARGS__)
//#define VERBOSE(fmt, ...)		DEBUGSERIAL.printf(fmt, ##__VA_ARGS__)

#define VERBOSEON(fmt, ...)		DEBUGSERIAL.printf(fmt, ##__VA_ARGS__)
#define MESSAGE(fmt, ...)		ioStreamPrintf(NULL, fmt, ##__VA_ARGS__)
#define CLIENTMSG(fmt, ...)		ioStreamPrintf(client, fmt, ##__VA_ARGS__)

#ifndef DEBUG
#define DEBUG(fmt, ...) ((void)0)
#endif
#ifndef VERBOSE
#define VERBOSE(fmt, ...) ((void)0)
#endif
#ifndef MESSAGE
#define MESSAGE(fmt, ...) ((void)0)
#endif
#ifndef CLIENTMSG
#define CLIENTMSG(fmt, ...) ((void)0)
#endif



// there are mixed reports of a serial flush problem. it was encountered on on eof the test boards used during development
#define SERIAL_FLUSH(s) s.flush()
//#define SERIAL_FLUSH(s) while(s.available()) s.read()

#include "pins.h"
#include "io.h"
#include "buttons.h"
#include "filesys.h"
#include "config.h"
#include "hexfile.h"
#include "wifi_net.h"
#include "cmds.h"
#include "smoketest.h"
