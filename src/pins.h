#ifndef __PINS_H
#define __PINS_H

/* ***************************************************************************
* File:    pins.h
* Date:    2019.09.09
* Author:  Bradan Lane Studio
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------

## HARDWARE

The PortaProg uses the LILYGO TTGO T-Display with an enhancement receiver board.

The LILYGO TTGO T-Display has a 240x135 ST7789V display and support for a LiPo battery connected through a JST PH 2.0 connector.

The receiver board breaks into the power management circuit and adds a power off switch.
A 3-way lever button switch provides for menu navigation and scrolling of the viewport.
The PortaProg programming and UART capabilities are exposed through a standard dual row pin header.

An additional 3-way lever button switch is available for scripted actions.
These buttons may be assigned script command strings to execute when pressed.
The 3 buttons are assigned command strings through the `.config` file as `b1script`, `b2script`, and `b3script`.

**NOTE:** The receiver board has a 4.7K Ohm resistor for the UPDI programming interface. It also has pullup resistors to make the GPI34..39 usable as input for the buttons.

*For historical value ... * During the development of the PortaProg one notable issue was with GPIO25.
Use of this pin caused crashes, Seria11.end() to hang, and I2C errors.
The issue with GPIO 25 and 26 is the DAC. To use GPIO25 and GPIO26, the DAC must first be disabled.

--- */

// NOTE: For portability, we reference pins by their GPIO# and not the board specific identifiers.


/* ---
## GPIO PIN REQUIREMENTS

The ESP32 has three hardware serial (UART) interfaces. The PortaProg uses all three:
 - Serial0 is used to interface to the computer through the USB (use the default serial for this) for debugging PortaProg
 - Serial1 is used to support UART communications to an attached chip - useful if your program uses UART for debugging or features (this serial interface is accessible thru the PortaProg's command set and over a Telnet connection)
 - Serial2 is used for UPDI - a 4.7K resistor is connected between TX & RX and the RX pin (*the unified pin*) connects to UPDI of the chip

The ESP32 alternate SPI is used to interface to ATTiny and ATMega chips.

The Receiver board has a 3-way navigation for menus and for scrolling the viewport. The center long press will **Select** the menu option or act as an **Return** from text mode.

The TTGO T-Display has 2-buttons on the board. For convenience, these are replicated to the receiver board as action buttons **up** and **down**.

In cases where the chip should be powered off after programming, There are `POWER` commands within the command set.

--- */


// Nearly every available pin is used. below are the pins used, ordered as they are broken out from the TTGO:

// TTGO T-Display left/bottom pin row
// GND
// GND
#define PIN_I2C_SDA			21
#define PIN_I2C_SCL			22
#define PIN_PWR_SW			17
#define PIN_SPI_RESET		2	// OOPS - it turns out the ESP32 will not boot if an SPI device is attached because this pin needs to be floating
#define PIN_SPI_CLK			15
#define PIN_SPI_MISO    	13
#define PIN_SPI_MOSI    	12
// GND
// GND
// 3V

// right/top pin row
// 3V
#define PIN_BUTTON_UP		36	// warning: input only (SW1)
#define PIN_BUTTON_CENTER	37	// warning: input only (SW1)
#define PIN_BUTTON_DOWN		38	// warning: input only (SW1)
//#define PIN_BUTTON_EX2		39	// warning: input only (SW2)
#define PIN_UART2_TX		32
#define PIN_UART2_RX		33
// GPIO25 						// warning: must disable DAC to use; we expose this on the header in case the user wants it
#define PIN_UPDI_TX			26 // warning: must disable DAC to use
#define PIN_UPDI_RX	    	27
// GND
// 5V

// pins not exposed as pins
//#define PIN_BUTTON_EX3		35	// on the board; input only (SW2 via a jumper wire)
//#define PIN_BUTTON_EX1		0	// on the board;            (SW2 via a jumper wire)
#define NA_UART0_TX			1	// used by the board
#define NA_UART0_RX			3	// used by the board
#define NA_DISP_LIGHT		4	// used by the ST7789
#define NA_DISP_CS			5	// used by the ST7789
#define NA_DISP_DC	    	16	// used by the ST7789
#define NA_DISP_CLK 		18	// used by the ST7789
#define NA_DISP_MOSI    	19	// used by the ST7789
#define NA_DISP_RESET   	23	// used by the ST7789
#define NA_ADC_ENABLE		14	// used by the board; high when on USB
#define NA_BATTERY_ADC		34	// used by the board

// the following is because I messed up the V5 PCB and routed to the wrong buttons
#if VERSION5
#define PIN_BUTTON_EX1		39	// warning: input only (SW2)
#define PIN_BUTTON_EX2		35	// on the board; input only (SW2 via a jumper wire)
#define PIN_BUTTON_EX3		0	// on the board;            (SW2 via a jumper wire)
#else // VERSION4
#define PIN_BUTTON_EX2		39	// warning: input only (SW2)
#define PIN_BUTTON_EX3		35	// on the board; input only (SW2 via a jumper wire)
#define PIN_BUTTON_EX1		0	// on the board;            (SW2 via a jumper wire)
#endif

#endif	// end of include file
