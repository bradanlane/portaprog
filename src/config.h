#ifndef __CONFIG_H__
#define __CONFIG_H__

#define MAX_BUTTON_STRING 127

extern const char *configWifiSSID;
extern const char *configWifiPassword;
extern int configWifiPort;
extern long configSerialBaud;
extern int configSerialTimeout;
extern bool configPower;
extern const char *configScript;
extern char configButton1[];
extern char configButton2[];
extern char configButton3[];

bool configRead();
bool configWrite();

#endif // __CONFIG_H__
