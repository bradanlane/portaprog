#ifndef __BUTTON_H
#define __BUTTON_H

#define BUTTON_IDLE			0
#define BUTTON_PRESS		1
#define BUTTON_LONG_PRESS	2
#define BUTTON_RELEASE		3
#define BUTTON_LONG_RELEASE 4
#define BUTTON_UNKNOWN		99

#define LONG_PRESS_DURATION 750 // milliseconds

#define BUTTON_MAX 6
const uint8_t g_button_pins[BUTTON_MAX] = {PIN_BUTTON_CENTER, PIN_BUTTON_UP, PIN_BUTTON_DOWN, PIN_BUTTON_EX1, PIN_BUTTON_EX2, PIN_BUTTON_EX3 };
#define BUTTON_CENTER 0 // indexes used for code readability
#define BUTTON_UP	  1 // indexes used for code readability
#define BUTTON_DOWN	  2 // indexes used for code readability
// TODO need to update to second lever switch
#define BUTTON_EX1    3
#define BUTTON_EX2    4
#define BUTTON_EX3    5


bool	buttonsInit();
bool	buttonsUpdate();
uint8_t	buttonSetState(uint8_t button_index, uint8_t state);
uint8_t	buttonGetState(uint8_t button_index);
bool	buttonIsActive(uint8_t button_index);

#endif

