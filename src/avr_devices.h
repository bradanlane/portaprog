#pragma once


// --------------------------------------------------------------------------------------------------------
// UPDI device data structures and assignment functions
// --------------------------------------------------------------------------------------------------------

#define AVR_PAGESIZE_MAX 512	// it was 128 until the new AVR DA and DB chips

// non-volitile memory version
#define NVM_VERSION_0 1
#define NVM_VERSION_2 2
#define NVM_VERSION_4 4

// AVR device types
enum {
	AVR8X_TINY_2X = 0,
	AVR8X_TINY_4X,
	AVR8X_TINY_8X,
	AVR8X_TINY_16X,
	AVR8X_MEGA_320,
	AVR8X_MEGA_321,
	AVR8X_MEGA_480,
	AVR8X_AVR_32DA,
	AVR8X_AVR_64DA,
	AVR8X_AVR_128DA,
	AVR8X_AVR_16DD,
	AVR8X_AVR_32DD,
	AVR8X_AVR_64DD,
	AVR8X_AVR_16DU,
	AVR8X_AVR_32DU,
	AVR8X_AVR_64DU,
};

typedef struct {
	uint8_t config_type;
	uint32_t flash_start;
	uint32_t flash_size;
	uint16_t flash_pagesize;
	uint16_t eeprom_size;
	uint32_t sigrow_start;	// was 0x1100
	uint32_t fuses_start;	// was 0x1280
} DeviceConfiguration;

static DeviceConfiguration g_device_configs[] = {
	//            flash addr,   flash size, page size, eeprom size,  sigrow addr, fuses addr
	{AVR8X_TINY_2X,   0x8000,     2 * 1024,        64,          64,       0x1100, 0x1280},
	{AVR8X_TINY_4X,   0x8000,     4 * 1024,        64,         128,       0x1100, 0x1280},
	{AVR8X_TINY_8X,   0x8000,     8 * 1024,        64,         128,       0x1100, 0x1280},
	{AVR8X_TINY_16X,  0x8000,    16 * 1024,        64,         256,       0x1100, 0x1280},
	// these are all odd balls
	{AVR8X_MEGA_320,  0x4000,    32 * 1024,       128,         256,       0x1100, 0x1280},
	{AVR8X_MEGA_321,  0x8000,    32 * 1024,       128,         256,       0x1100, 0x1280},
	{AVR8X_MEGA_480,  0x4000,    48 * 1024,       128,         256,       0x1100, 0x1280},
	{AVR8X_AVR_32DA,  0x800000,  32 * 1024,       512,         512,       0x1100, 0x1280},
	{AVR8X_AVR_64DA,  0x800000,  64 * 1024,       256,         512,       0x1100, 0x1280},	// the page size is actually 512 but it seems, since we erase flash first, we can write 256 at a time
	{AVR8X_AVR_128DA, 0x800000, 128 * 1024,       512,         512,       0x1100, 0x1280},
	// these are the current generations
	{AVR8X_AVR_16DD,  0x800000,  16 * 1024,       256,         256,       0x1100, 0x1050},	// the page size is actually 512 but it seems, since we erse flash first, we can write 256 at a time
	{AVR8X_AVR_32DD,  0x800000,  32 * 1024,       256,         256,       0x1100, 0x1050},	// the page size is actually 512 but it seems, since we erse flash first, we can write 256 at a time
	{AVR8X_AVR_64DD,  0x800000,  64 * 1024,       256,         256,       0x1100, 0x1050},	// the page size is actually 512 but it seems, since we erse flash first, we can write 256 at a time
	{AVR8X_AVR_16DU,  0x800000,  16 * 1024,       256,         256,       0x1080, 0x1050},	// the page size is actually 512 but it seems, since we erse flash first, we can write 256 at a time
	{AVR8X_AVR_32DU,  0x800000,  32 * 1024,       256,         256,       0x1080, 0x1050},	// the page size is actually 512 but it seems, since we erse flash first, we can write 256 at a time
	{AVR8X_AVR_64DU,  0x800000,  64 * 1024,       256,         256,       0x1080, 0x1050},	// the page size is actually 512 but it seems, since we erse flash first, we can write 256 at a time
	};

typedef struct {
	uint16_t signature; // all signatures begin with 0x1E and then 2 bytes
	char shortname[8];
	char longname[16];
	uint8_t config_type;
} DeviceIdentification;

DeviceIdentification g_updi_devices[] = {
	//	signature, short id, descriptive name, config
	{0x9123, "t202",   "ATtiny202",  AVR8X_TINY_2X},
	{0x9122, "t204",   "ATtiny204",  AVR8X_TINY_2X},
	{0x9121, "t212",   "ATtiny212",  AVR8X_TINY_2X},
	{0x9120, "t214",   "ATtiny214",  AVR8X_TINY_2X},
	{0x9223, "t402",   "ATtiny402",  AVR8X_TINY_4X},
	{0x9226, "t404",   "ATtiny404",  AVR8X_TINY_4X},
	{0x9225, "t406",   "ATtiny406",  AVR8X_TINY_4X},
	{0x9223, "t412",   "ATtiny412",  AVR8X_TINY_4X},
	{0x9222, "t414",   "ATtiny414",  AVR8X_TINY_4X},
	{0x9221, "t416",   "ATtiny416",  AVR8X_TINY_4X},
	{0x9220, "t417",   "ATtiny417",  AVR8X_TINY_4X},
	{0x9325, "t804",   "ATtiny804",  AVR8X_TINY_8X},
	{0x9324, "t806",   "ATtiny806",  AVR8X_TINY_8X},
	{0x9323, "t807",   "ATtiny807",  AVR8X_TINY_8X},
	{0x9322, "t814",   "ATtiny814",  AVR8X_TINY_8X},
	{0x9321, "t816",   "ATtiny816",  AVR8X_TINY_8X},
	{0x9320, "t817",   "ATtiny817",  AVR8X_TINY_8X},
	{0x9425, "t1604",  "ATtiny1604", AVR8X_TINY_16X},
	{0x9424, "t1606",  "ATtiny1606", AVR8X_TINY_16X},
	{0x9423, "t1607",  "ATtiny1607", AVR8X_TINY_16X},
	{0x9422, "t1614",  "ATtiny1614", AVR8X_TINY_16X},
	{0x9421, "t1616",  "ATtiny1616", AVR8X_TINY_16X},
	{0x9420, "t1617",  "ATtiny1617", AVR8X_TINY_16X},
	{0x9552, "m3208",  "ATmega3208", AVR8X_MEGA_320},
	{0x9553, "m3209",  "ATmega3209", AVR8X_MEGA_320},
	{0x9520, "t3214",  "ATtiny3214", AVR8X_MEGA_321},
	{0x9521, "t3216",  "ATtiny3216", AVR8X_MEGA_321},
	{0x9522, "t3217",  "ATtiny3217", AVR8X_MEGA_321},
	{0x9650, "m4808",  "ATmega4808", AVR8X_MEGA_480},
	{0x9651, "m4809",  "ATmega4809", AVR8X_MEGA_480},
	{0x9615, "r64da28", "AVR64DA28", AVR8X_AVR_64DA},
	{0x9614, "r64da32", "AVR64DA32", AVR8X_AVR_64DA},
	{0x9613, "r64da48", "AVR64DA48", AVR8X_AVR_64DA},
	{0x9612, "r64da64", "AVR64DA64", AVR8X_AVR_64DA},

	{0x9431, "r16dd32", "AVR16DD32", AVR8X_AVR_16DD},
	{0x9432, "r16dd28", "AVR16DD28", AVR8X_AVR_16DD},
	{0x9433, "r16dd20", "AVR16DD20", AVR8X_AVR_16DD},
	{0x9434, "r16dd14", "AVR16DD14", AVR8X_AVR_16DD},
	{0x9538, "r32dd32", "AVR32DD32", AVR8X_AVR_32DD},
	{0x9539, "r32dd28", "AVR32DD28", AVR8X_AVR_32DD},
	{0x953A, "r32dd20", "AVR32DD20", AVR8X_AVR_32DD},
	{0x953B, "r32dd14", "AVR32DD14", AVR8X_AVR_32DD},
	{0x961A, "r64dd32", "AVR64DD32", AVR8X_AVR_64DD},
	{0x961B, "r64dd28", "AVR64DD28", AVR8X_AVR_64DD},
	{0x961C, "r64dd20", "AVR64DD20", AVR8X_AVR_64DD},
	{0x961D, "r64dd14", "AVR64DD14", AVR8X_AVR_64DD},

	{0x9438, "r16du32", "AVR16DU32", AVR8X_AVR_16DU},
	{0x9439, "r16du28", "AVR16DU28", AVR8X_AVR_16DU},
	{0x943A, "r16du20", "AVR16DU20", AVR8X_AVR_16DU},
	{0x943B, "r16du14", "AVR16DU14", AVR8X_AVR_16DU},
	{0x953F, "r32du32", "AVR32DU32", AVR8X_AVR_32DU},
	{0x9540, "r32du28", "AVR32DU28", AVR8X_AVR_32DU},
	{0x954E, "r32du20", "AVR32DU20", AVR8X_AVR_32DU},
	{0x954F, "r32du14", "AVR32DU14", AVR8X_AVR_32DU},
	{0x9621, "r64du32", "AVR64DU32", AVR8X_AVR_64DU},
	{0x9622, "r64du28", "AVR64DU28", AVR8X_AVR_64DU},
	};
