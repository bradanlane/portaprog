// -------- AVR UPDI functions --------------------------------------------------------------------
bool updiInit();
void updiLoop();
bool updiIsConnected(Stream* client, bool silent);
bool updiGetInfo(Stream *client);
bool updiReadFuses(Stream *client);
bool updiWriteFuse(Stream *client, uint8_t fuse_number, uint8_t fuse_value);
bool updiFlashData(Stream *client,ihexStream* src);
bool updiDumpFlash(Stream *client, ihexStream* dest);
bool updiErase(Stream *client);
bool updiWriteEeprom(Stream *client, uint32_t offset, ihexStream* src);
bool updiReadEeprom(Stream *client, uint32_t offset, uint32_t size, ihexStream* dest);

