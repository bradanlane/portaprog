#ifndef __DISPLAY_H
#define __DISPLAY_H

#define MAX_LINE_TEXT 512 // number of characters we will allow for a wrapping line of text

#define DISPLAY_ICON_STATE_UNK 	-1
#define DISPLAY_ICON_STATE_OFF 	 0
#define DISPLAY_ICON_STATE_ON	 1


bool ioInit();
void ioLoop();
void ioClear(bool home);
void ioWrapEnable(bool);
void ioIcons(int8_t rx, int8_t tx, int8_t con);
int ioPrint(const char *text);
int ioStreamPrint(Stream *out, const char *line);
int ioStreamPrintf(Stream *out, const char *fmt, ...);
const char *ioGetBatteryInfo();
void ioWaitForButton(bool);
bool ioRunCommandLine(const char *commands, bool aborted, bool wait);
bool ioRunCommandFile(const char *filename, bool wait);
bool ioRunCommandPrintf(const char *fmt, ...);

bool ioIsMute();
void ioMuteOn();
void ioMuteOff();

#endif
