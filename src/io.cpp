/* ***************************************************************************
* File:    io.cpp
* Date:    2020.08.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------

### I/O

The PortaProg combines a buffer stream and screen stream into a I/O stream.
The screen stream also handles button state(s).
To prevent inadvertent activation, the `SELECT` or `EXECUTE` action requires a long press.

The display and UI interface supports text-line based display functions.

The use of various graphics - such as a progress bar - could be future enhancements.

There display has three modes:

- file list - a list of the files currently in SPIFFS storage
- file menu - list of options available for the currently selected file _(1)_
- text msg - a scrolling screen of messages for the current operations

The menu mode is context sensitive to teh file type selected.
When the file type is "hex", the menu includes the option to `FLASH`.
When the file type is "cmd", the menu includes the option to `RUN`.

**NOTE:** Command files are limited to 8K in size. Single command lines are limited to 256 bytes in size.
--- */

// **NOTE:** the coordinate system has 0,0 in the upper left. Text operations for the OLED reference the bottom left of the character.

// now the local includes
#include "allincludes.h"
#include <driver/rtc_io.h>
#include "esp_adc_cal.h"

#include "screenstream.h"
#include "bufferstream.h"

#define MAX_INPUT_BUFFER (8 * 1024) // a command string or the contents of a command file cannot exceed this size

class ioStream : public Stream {
	bufferStream *in;
	screenStream *out;
	char output_line_buffer[MAX_LINE_TEXT + 1];

  public:
	ioStream(bufferStream *ins, screenStream *outs) {
		in = ins;
		out = outs;
		memset(output_line_buffer, 0, MAX_LINE_TEXT + 1);
	}

	/** Clear the buffers */
	void clear() {
		in->clear();
		out->clear(true);
	}

	virtual size_t write(uint8_t b) {
		return out->write(b);
	}

	virtual size_t print(const char *text) {
		VERBOSE("ioStream::print (%s)\n", text);
		return out->print(text);
	}

	virtual size_t printf(const char *fmt, ...) {
		VERBOSE("ioStream::printf ()\n");
		va_list args;
		va_start(args, fmt);

		this->output_line_buffer[0] = 0;
		vsnprintf(this->output_line_buffer, MAX_LINE_TEXT, fmt, args);
		this->output_line_buffer[MAX_LINE_TEXT - 1] = 0;
		va_end(args);

		return out->print(this->output_line_buffer);
	}

	virtual int availableForWrite(void) {
		return out->availableForWrite();
	}

	virtual void flush() {
		out->flush();
		in->clear();
	}

	virtual int available() {
		return in->available();
	}
	virtual int read() {
		return in->read();
	}
	virtual int peek() {
		return in->peek();
	}
};

static screenStream *g_screen_stream;
static bufferStream *g_buffer_stream;
static ioStream *g_io_stream;
static bool g_io_waiting_for_button;
static bool g_io_quiet;

/* ---
#### ioInit()

Performs all necessary initialization of both the display and the button(s)
It must be called once before using any of the other display functions.

return: **bool** `true` on success and `false` on failure
--- */
bool ioInit(void) {
	buttonsInit();
	g_io_waiting_for_button = false;
	g_io_quiet = false;

	g_screen_stream = NULL;
	g_screen_stream = new screenStream(MAX_LINE_TEXT);
	if (!g_screen_stream) {
		MESSAGE("ERROR: failed to create Screen Stream ... so we are pretty much SOL\n");
	} else if (g_screen_stream->availableForWrite() != MAX_LINE_TEXT) {
		MESSAGE("ERROR: Display buffer is %d bytes of %d bytes\n", g_screen_stream->availableForWrite(), MAX_LINE_TEXT);
	}

	g_buffer_stream = NULL;
	g_buffer_stream = new bufferStream(MAX_INPUT_BUFFER);
	if (!g_buffer_stream) {
		MESSAGE("ERROR: failed to create Buffer Stream ... so we are pretty much SOL\n");
	} else if (g_buffer_stream->availableForWrite() != MAX_INPUT_BUFFER) {
		MESSAGE("ERROR: Input buffer is %d bytes of %d bytes\n", g_buffer_stream->availableForWrite(), MAX_INPUT_BUFFER);
	}

	g_io_stream = NULL;
	g_io_stream = new ioStream(g_buffer_stream, g_screen_stream);
	if (!g_io_stream) {
		MESSAGE("ERROR: failed to create I/O Stream ... so we are pretty much SOL\n");
	}

	MESSAGE("Battery: %s\n", ioGetBatteryInfo());
	return true;
}

/* ---
#### ioLoop()

Give the display and button(s) an opportunity to respond to interaction events and update the display
--- */
void ioLoop(void) {

	if (buttonsUpdate()) {
		VERBOSE("ioLoop() buttons changed\n");

	// in most cases we are watching for button releases

// to simplify readability of the code, we setup a variable here
#define BUTTON_ACTION_SELECT 1
#define BUTTON_ACTION_PREV	 2
#define BUTTON_ACTION_NEXT	 3

		// to mimic continuous scroll, we toggle LONG_PRESS back to PRESS so it will keep happening
		if (buttonGetState(BUTTON_UP) == BUTTON_LONG_PRESS) buttonSetState(BUTTON_UP, BUTTON_PRESS);
		if (buttonGetState(BUTTON_DOWN) == BUTTON_LONG_PRESS) buttonSetState(BUTTON_DOWN, BUTTON_PRESS);


		if (!g_screen_stream->isTextMode()) {
			g_screen_stream->delayTextMode(true);	// delay text mode from overrunning the user interaction with the menus

			if (buttonGetState(BUTTON_DOWN) == BUTTON_PRESS)
				g_screen_stream->updateListPosition(+1);
			else if (buttonGetState(BUTTON_UP) == BUTTON_PRESS)
				g_screen_stream->updateListPosition(-1);
			else if (buttonGetState(BUTTON_CENTER) == BUTTON_LONG_PRESS)
				g_screen_stream->selectCurrent();
		} else { // TEXT_MODE
			if (buttonGetState(BUTTON_CENTER) == BUTTON_LONG_PRESS) {
				g_screen_stream->clear(true);
			} else {
				// in text mode, if we have the multiple buttons, we can pan around the virtual screen
				if (buttonGetState(BUTTON_UP) == BUTTON_PRESS)
					g_screen_stream->scrollUp();
				else if (buttonGetState(BUTTON_DOWN) == BUTTON_PRESS)
					g_screen_stream->scrollDown();
			}
		}

		// now check if any of the action command buttons have been pressed
		if (buttonGetState(BUTTON_EX2) == BUTTON_PRESS) {
			VERBOSE("Action Button 2 pressed = [%s]\n", configButton2);
			g_screen_stream->delayTextMode(false);	// allow immediate text mode
			ioRunCommandLine(configButton2, false, !ioIsMute());
		}
		if (buttonGetState(BUTTON_EX1) == BUTTON_PRESS) {
			VERBOSE("Action Button 1 pressed = [%s]\n", configButton1);
			g_screen_stream->delayTextMode(false);	// allow immediate text mode
			ioRunCommandLine(configButton1, false, !ioIsMute());
		}
		if (buttonGetState(BUTTON_EX3) == BUTTON_PRESS) {
			VERBOSE("Action Button 3 pressed = [%s]\n", configButton3);
			g_screen_stream->delayTextMode(false);	// allow immediate text mode
			ioRunCommandLine(configButton3, false, !ioIsMute());
		}
	} else {
		// nothing here anymore; this use to be where the 'forced' sleep mode was activated
	}
	g_screen_stream->refresh();
	//delay(1);
}

// TODO markdown doc for the rest of the io functions

void ioClear(bool home) {
	g_screen_stream->clear(home);
}

/* --
#### ioIsMute()

Indicate if the portaprog display is operating in quiet mode - aka supressing status messages.

- output: **bool** true if status messages should be supressed
-- */

bool ioIsMute() {
	return g_io_quiet;
}

void ioMuteOn() {
	g_io_quiet = true;
}

void ioMuteOff() {
	g_io_quiet = false;
}


void ioWrapEnable(bool on) {
	if (on)
		g_screen_stream->wrapOn();
	else
		g_screen_stream->wrapOff();
}

void ioIcons(int8_t rx, int8_t tx, int8_t con) {
	g_screen_stream->icons(rx, tx, con);
}

int ioPrint(const char *text) {
	if (g_io_waiting_for_button) {
		// for esthetics, we know the BUTTON message has no newline at the end so we add it in now before more output
		g_io_waiting_for_button = false;
		DEBUGSERIAL.print("\n");
		g_screen_stream->print("\n");
	}
	DEBUGSERIAL.print(text); // most likely any formal output should also go to the debug screen.

	// if we are quiet to the display, we can return now
	if (ioIsMute())
		return true;

	return g_screen_stream->print(text);
}

int ioStreamPrint(Stream *out, const char *line) {
	if ((out != NULL) && (out != g_io_stream) && (out != g_screen_stream))
		out->print(line);

	return ioPrint(line);
}

int ioStreamPrintf(Stream *out, const char *fmt...) {
	static char line[MAX_LINE_TEXT + 1];
	va_list args;
	va_start(args, fmt);

	line[0] = 0;
	vsnprintf(line, MAX_LINE_TEXT, fmt, args);
	line[MAX_LINE_TEXT - 1] = 0;
	va_end(args);

	if ((out != NULL) && (out != g_io_stream) && (out != g_screen_stream))
		out->print(line);

	return ioPrint(line);
}

bool ioRunCommandLine(const char *line, bool aborted, bool wait) {
	bool success = true;
	if (line && strlen(line) > 0) {
		MESSAGE("CMD: %s\n", line);

		//g_screen_stream->clear(true);
		// store the commands to the input buffer
		if (g_buffer_stream != NULL)
			g_buffer_stream->print(line);

		// BUG ? we need to process the commands "one line at a time"
		success = cmdsProcessCommands(g_io_stream, aborted);
		if (wait)
			ioWaitForButton(true);
	}
	return success;
}

bool ioRunCommandPrintf(const char *fmt, ...) {
	static char line[MAX_LINE_TEXT + 1];
	va_list args;
	va_start(args, fmt);

	line[0] = 0;
	vsnprintf(line, MAX_LINE_TEXT, fmt, args);
	line[MAX_LINE_TEXT - 1] = 0;
	va_end(args);
	return ioRunCommandLine(line, false, !ioIsMute());
}

bool ioRunCommandFile(const char *name, bool wait) {
	// open file. write contents into g_buffer_stream. close file. call cmdsProcessCommands(g_io_stream);
	bool success = true;
	File f = filesysOpen(name, "r");

	if (f) {
		MESSAGE("Run Command File: %s\n", name);

		while (f.available()) {
			// we need to process the commands "one line at a time"
			char line[MAX_LINE_TEXT + 1];
			uint16_t len = streamReadLine((Stream *)(&f), line, MAX_LINE_TEXT, false);
			if (len) {
				FILESYS_STRIP_NL(line, len);
				if (len) {
					success = ioRunCommandLine(line, !success, false);
				}
			}
		}
		filesysClose(f);
	} else {
		MESSAGE("Unable to run command file %s\n", name);
	}
	if (wait)
		ioWaitForButton(true);
	return true;
}

const char *ioGetBatteryInfo() {

#if 0 // used to debug actual VREF
	int vref;
	esp_adc_cal_characteristics_t adc_chars;
	esp_adc_cal_value_t val_type = esp_adc_cal_characterize((adc_unit_t)ADC_UNIT_1, (adc_atten_t)ADC1_CHANNEL_6, (adc_bits_width_t)ADC_WIDTH_BIT_12, 1100, &adc_chars);
    //Check type of calibration value used to characterize ADC
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        Serial.printf("eFuse Vref:%u mV", adc_chars.vref);
        vref = adc_chars.vref;
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
        Serial.printf("Two Point --> coeff_a:%umV coeff_b:%umV\n", adc_chars.coeff_a, adc_chars.coeff_b);
    } else {
        Serial.println("Default Vref: 1100mV");
    }
#endif

#define BATTERY_VREF 1150 // default is 1100  .. .this value is just a WAG (sorry)
	static char message[64];
	message[0] = 0;
	uint16_t v = analogRead(NA_BATTERY_ADC);
	//DEBUGSERIAL.printf("analogRead() = %d\n", v);
	float battery_voltage = 0;

	// battery_voltage = ((float)v / 4095.0) * 2.0 * 3.3 * (BATTERY_VREF / 1000.0);
	battery_voltage = ((float)v / 4095.0) * 2.0 * 3.0 * (BATTERY_VREF / 1000.0);

	// KLUDGE when it's on USB, the reported voltage is too high
	if (battery_voltage > 4.3) {
		battery_voltage = battery_voltage * 0.875;
	}

#if 0
	// round to 0.1
	battery_voltage = round((battery_voltage * 10)) / 10;
	snprintf(message, 64, "%3.1fV", battery_voltage);
#else
	// round to 0.05
	battery_voltage = round((battery_voltage * 20)) / 20;
	snprintf(message, 64, "%4.2fV", battery_voltage);
#endif
	return message;
}

/* ---
#### ioWaitForButton()

Wait for a button press/release before continuing

The button state is ignored until it is IDLE then waits for the button release event. The function only returns once the button RELEASE has been detected.

**Note**: This is a _polling function, not interrupt driven_ and this is a _blocking operation_.
--- */

void ioWaitForButton(bool prompt) {
	// flush any button events
	buttonsUpdate();

	// no need to duplicate the message
	if (g_io_waiting_for_button)
		return;

	if (prompt)
		MESSAGE("`---- PUSH THE BUTTON ----");

#if 0	// this quick test didn't work ??
	// void tone(byte pin, int freq)
	{
		ledcSetup(0, 420, 8);			// setup beeper
		ledcAttachPin(25 /*pin*/, 0);	// attach beeper
  		ledcWriteTone(0, 420 /*freq*/);	// play tone
		delay(1000);
  		ledcWriteTone(0, 0 /*freq*/);	// tone off
	}
#endif

	// we wait for a button press
	g_screen_stream->timeout(IDLE_USER_TIMEOUT); // we take advantage of the screen Stream's timeout timer
	g_io_waiting_for_button = true;
#if 0
	VERBOSE("ioWaitForButton() waiting for button press\n");
	while (!buttonsUpdate() && !g_screen_stream->timeout(IDLE_TIMEOUT_TEST))
		;

	VERBOSE("ioWaitForButton() waiting for button release\n");
	while (!buttonsUpdate() && !g_screen_stream->timeout(IDLE_TIMEOUT_TEST))
		;

	// flush any button events
	buttonsUpdate();

	g_screen_stream->clear(true);
#endif
}
