#ifndef __BUFFERSTREAM_H
#define __BUFFERSTREAM_H

// by tracking 'stored_size' and 'unread_size' we are able to rewind the buffer repeat using it
class bufferStream : public Stream {
  private:
	// the input (buffer) properties
	uint8_t *buffer;
	int32_t buffer_size, stored_size;
	int32_t buffer_pos, unread_size;
	uint16_t errors;

  public:
	static const uint32_t DEFAULT_SIZE = 256;

	bufferStream(uint32_t buffer_size = bufferStream::DEFAULT_SIZE);
	~bufferStream();

	// operations to change internal buffer
	void clear();

	// read from internal buffer
	virtual int32_t available();
	virtual int peek();
	virtual int read();


	virtual void flush();
	virtual int availableForWrite(void);
	virtual size_t write(uint8_t);
	int32_t writeStream(Stream *src);
};

#endif
