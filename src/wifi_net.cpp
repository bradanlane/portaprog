/* ***************************************************************************
* File:    wifi.cpp
* Date:    2018.02.16
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined
* under the MIT License
*
* ******************************************************************************/

/* ---
--------------------------------------------------------------------------

### WIFI API

Provides wifi or hotspot access and enables over-the-air updates.

The interface also handles connections for TCP and TELNET.
--- */
#include <ArduinoOTA.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <functional>

// local includes
#include "allincludes.h"

#define TCP_TIMEOUT 500	// milliseconds

// #define ALLOW_TELNET	// TELNET support was legacy and deprecated

static WiFiMulti wifiMulti; // Create an instance of the WiFiMulti class, called 'wifiMulti'

static WiFiServer g_tcp_server(TCP_PORT); // TODO need a more secure option
#ifdef ALLOW_TELNET
static WiFiServer g_telnet_server(TELNET_PORT); // TODO need a more secure option
#endif

// we only support one client (of each type) at a time
#ifdef ALLOW_TELNET
static WiFiClient g_telnet_client;
#endif
WiFiClient g_tcp_client;

static WiFiMode_t g_wifi_mode = WIFI_AP;

#define mdnsRoot "PORTAPROG" // Domain name for the mDNS responder

static char mdnsName[32];			  // make it a composite of 'root the last hex value from the MAC address
static bool g_wifi_connected = false;
static bool g_wifi_is_hotspot = false;
char g_network_buf[MAX_NETWORK_TEXT + 1 + 1]; // buffer + '\n' + 0

/*__________________________________________________________SETUP_FUNCTIONS__________________________________________________________*/


static bool _wifi_start_tcp_server() {
	// Start Telnet server
	if (!configWifiPort)
		configWifiPort = TCP_PORT;

	g_tcp_server.begin(configWifiPort);
	g_tcp_server.setNoDelay(true);
	return true;
}


#ifdef ALLOW_TELNET
static bool _wifi_start_telnet_server() {
	// Start Telnet server
	g_telnet_server.begin();
	g_telnet_server.setNoDelay(true);
	return true;
}
#endif


static void _wifi_start_mdns() { // Start the mDNS responder
	if (!MDNS.begin(mdnsRoot)) // start the multicast domain name server
		MESSAGE("mDNS error");
	else {
		MDNS.addService("tcpupdi",	"tcp", TCP_PORT);
		MDNS.addService("telnet",	"tcp", TELNET_PORT);
		//MESSAGE("TCP://%s\n", wifiAddress());
	}
}


static bool _wifi_start_network() { // Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
	// int channel = random(12) + 1;
	uint8_t addr[6];
	char hotspot_name[32];
	bool state = true;
	int i = 0;

	g_wifi_connected = false;
	g_wifi_is_hotspot = false;

	WiFi.softAPmacAddress(addr);
	//sprintf(hotspot_name, "%s_%02x%02x%02x", mdnsRoot, addr[3], addr[4], addr[5]);
	//sprintf(mdnsName, "%s%02x", mdnsRoot, addr[5]);	// useful if there is likely to be more than one
	sprintf(hotspot_name, "%s", mdnsRoot);
	sprintf(mdnsName, "%s", mdnsRoot); // simple when there is only one

	if (strlen(configWifiSSID) && strlen(configWifiPassword)) {
		wifiMulti.addAP(configWifiSSID, configWifiPassword); // add Wi-Fi networks you want to connect to when available

		DEBUGSERIAL.print("Connecting ... ");
		unsigned long t = millis() + 10000;
		while (wifiMulti.run() != WL_CONNECTED && WiFi.softAPgetStationNum() < 1) { // Wait for a Wi-Fi connection
			DEBUGSERIAL.print(".");

			delay(500);
			// 20 attempts or 10 seconds (because wifiMulti is really slow when it can't find anything)
			if ((i++ > 20) || (millis() > t)) {
				state = false;
				break;
			}
		}
		DEBUGSERIAL.println("");
	}
	else
		state = false;

	if (state) {
		MESSAGE("%s ", WiFi.SSID().c_str());
		MESSAGE("%s\n", wifiAddress());
		g_wifi_connected = true;
	} else if (WiFi.softAP(hotspot_name /*, g_otaPassword *//*, channel */)) { // Start the access point, re-using the OAT password
		g_wifi_is_hotspot = true;
		g_wifi_connected = true;
		MESSAGE("hotspot %s", hotspot_name);
		MESSAGE(" %s\n", wifiAddress());
	} else {
		MESSAGE("failed to connect to access point\n");
		MESSAGE("failed to establish %s\n", hotspot_name);
	}

	g_wifi_mode = WiFi.getMode();

	return g_wifi_connected;
}


/*________________________________________________________PROCESSING_FUNCTIONS__________________________________________________________*/


static void _wifi_handle_tcp_requests() {
	// handle any connection requests
	// we only support one telnet connect at a time; a second one is ejected
	if (g_tcp_server.hasClient()) {
		VERBOSE("tcp client requested\n");
		if (g_tcp_client)
			g_tcp_client.stop();
		g_tcp_client = g_tcp_server.available();
	} else {
		// we lost the connection
		if (g_tcp_client && !g_tcp_client.connected()) {
			g_tcp_client.stop();
			VERBOSE("tcp client lost\n");
			return;
		}
	}

	if (g_tcp_client && g_tcp_client.connected()) {
		uint32_t timeout = millis() + TCP_TIMEOUT;
		while (millis() < timeout) {
			if (g_tcp_client.available())
				break;
		}
		if (millis() >= timeout) {
			// we timed out
			VERBOSE("TCP Client Timeout\n");
			g_tcp_client.stop();
			return;
		}
		// TCP processing is handed off to the avr subsystem
		cmdsProcessCommands((Stream *) (&g_tcp_client), false);
	}
	// since we operate in single command/response mode, we can close the client session
	g_tcp_client.stop();
}

#ifdef ALLOW_TELNET
static void _wifi_handle_telnet_requests() {
	memset(g_network_buf, 0, (MAX_NETWORK_TEXT + 1 + 1));
	// the telnet connection interfaces to the CHIPSERIAL UART

	// handle any connection requests
	// we only support one telnet connect at a time; a second one is ejected
	if (g_telnet_server.hasClient()) {
		//DEBUGSERIAL.println("telnet client requester");
		if (g_telnet_client)
			g_telnet_client.stop();
		g_telnet_client = g_telnet_server.available();
	} else {
		// we lost the connection
		if (g_telnet_client && !g_telnet_client.connected())
			g_telnet_client.stop();
	}

	if (!(g_telnet_client && g_telnet_client.connected())) {
		ioIcons(DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_OFF);
		return;
	}

	// a little trick: if the first character is a backslash, then we redirect the telnet to the avr command process
	if (g_telnet_client.available() && (g_telnet_client.peek() == '\\')) {
		g_telnet_client.read(); // throw away the back slash
		delay(1000);
		/*
			OK, this is a huge kludge.
			we delay so the user has time to compose and send the command(s).
			they (I) had better type quick .. or use cut/paste ;-)
		*/
		cmdsProcessCommands((Stream *)(&g_telnet_client), false);
		return;
	}

	ioIcons(DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_ON);

	// Get data from the telnet client and push it to the UART
	if (g_telnet_client.available())
		ioIcons(DISPLAY_ICON_STATE_ON, DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_UNK);

	while (g_telnet_client.available()) {
		CHIPSERIAL.write(g_telnet_client.read());
	}
	ioIcons(DISPLAY_ICON_STATE_OFF, DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_UNK);

	// get any data from UART and push it to telnet client
	if (CHIPSERIAL.available()) {
		size_t len = CHIPSERIAL.available();
		ioIcons(DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_ON, DISPLAY_ICON_STATE_UNK);

		if (len > MAX_NETWORK_TEXT)
			len = MAX_NETWORK_TEXT;

		CHIPSERIAL.readBytes(g_network_buf, len);

		// Push UART data to telnet client
		g_telnet_client.write(g_network_buf, len);

		// if the buffer doesn't have a newline, add one but be sure there is space
		if (g_network_buf[len-2] != '\n') {
			if (len == MAX_NETWORK_TEXT)
				len -= 2;
			g_network_buf[len-1] = '\n';
			g_network_buf[len] = 0;
		}
		else {
			// if the buffer is not null terminated, do so but be sure there is space
			if (g_network_buf[len-1] != 0) {
				if (len == MAX_NETWORK_TEXT)
					len--;
				g_network_buf[len] = 0;
			}
		}

		VERBOSE("\nbuf[%d]:\n", len);
		ioStreamPrint(NULL, g_network_buf);

		ioIcons(DISPLAY_ICON_STATE_UNK, DISPLAY_ICON_STATE_OFF, DISPLAY_ICON_STATE_UNK);
	}
}
#endif


/*________________________________________________________ PUBLIC_FUNCTIONS __________________________________________________________*/

/* ---
#### wifiOff()

Turn the WiFi off; it saves the current state
--- */
void wifiOff() {
	WiFiMode_t mode = WiFi.getMode();
	if (mode != WIFI_OFF)
		g_wifi_mode = mode;
	WiFi.mode(WIFI_OFF);
}

/* ---
#### wifiOn()

Turn the WiFi off; it saves the current state
--- */
void wifiOn() {
	if (g_wifi_mode != WIFI_OFF)
		WiFi.mode(g_wifi_mode);
}

/* ---
#### wifiIsConnected()

get the current availability of wifi connectivity - either via an existing access point or as a hotspot

return: **bool** `true` when wifi is available and `false` when not
--- */
bool wifiIsConnected() {
	return g_wifi_connected;
}

/* ---
#### wifiConnectionLost()

test is we still have wifi connectivity; if we never had wifi we can't lose something we never had

return: **bool** `true` if we lost a prior wifi connection and `false` if we are at the same state as before
--- */
bool wifiConnectionLost() {
	// we were connected but are no longer connected
	return (g_wifi_connected && (!g_wifi_is_hotspot) && (WiFi.status() != WL_CONNECTED));
}

/* ---
#### wifiIsHotspot()

indicate if the wifi connection is a hotspot or connected to an access point

return: **bool** `true` when wifi is a hotspot and `false` when it is an access point
--- */
bool wifiIsHotspot() {
	return g_wifi_is_hotspot;
}

/* ---
#### wifiAddress()

a convenience function to get the WiFi address as character string

return: **char* ** static internal buffer of the wifi address in the form nnn.nnn.nnn.nnn
--- */
char *wifiAddress() {
#define IPADDRSIZE 16
	static char buffer[IPADDRSIZE + 1];
	IPAddress ip;
	if (g_wifi_is_hotspot)
		ip = WiFi.softAPIP();
	else
		ip = WiFi.localIP();
	snprintf(buffer, IPADDRSIZE, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
	return buffer;
}

/* ---
#### wifiAddressEnding()

a convenience function to get the last value in the WiFi address

return: **uint8_t** last number in wifi address eg xxx.xxx.xxx.NNN
--- */
uint8_t wifiAddressEnding() {
	IPAddress ip;
	if (g_wifi_is_hotspot)
		ip = WiFi.softAPIP();
	else
		ip = WiFi.localIP();
	return ip[3];
}


/* ---
#### wifiInit()

Performs all necessary initialization. Must be called once before using any of the other wifi functions.

return: **bool** `true` on success and `false` on failure
--- */
bool wifiInit() {
	g_wifi_connected = _wifi_start_network(); // Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
	VERBOSE("IP: %s\n", wifiAddress());
	_wifi_start_mdns();					   // Start the mDNS responder
	bool success = g_wifi_connected;
	success &= _wifi_start_tcp_server();
#ifdef ALLOW_TELNET
	success &= _wifi_start_telnet_server();
#endif
	return (success);
}

/* ---
#### wifiLoop()

Give the WiFi services an opportunity to respond to any necessary actions
--- */
void wifiLoop() {
#ifdef ALLOW_TELNET
	_wifi_handle_telnet_requests();
#endif
	_wifi_handle_tcp_requests();
}
